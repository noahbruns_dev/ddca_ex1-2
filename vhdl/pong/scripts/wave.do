onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pong_tb/tb/clk
add wave -noupdate /pong_tb/tb/instr
add wave -noupdate /pong_tb/tb/instr_wr
add wave -noupdate /pong_tb/tb/instr_full
add wave -noupdate /pong_tb/tb/frame_sync
add wave -noupdate /pong_tb/tb/drawing_color
add wave -noupdate /pong_tb/tb/player1_points
add wave -noupdate /pong_tb/tb/player2_points
add wave -noupdate /pong_tb/tb/game_state
add wave -noupdate /pong_tb/tb/synth_cntrl
add wave -noupdate /pong_tb/tb/player1_input
add wave -noupdate /pong_tb/tb/player2_input
add wave -noupdate /pong_tb/tb/pong_state
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {60000310000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits us
update
WaveRestoreZoom {59999989019 ps} {60003193371 ps}
