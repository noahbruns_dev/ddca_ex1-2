library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pong_pkg.all;
use work.snes_cntrl_pkg.all;

entity pong_tb is
end entity;

architecture beh of pong_tb is
	constant CLK_PERIOD : time := 20 ns;
	signal clk      : std_logic;
	signal stop_clock : boolean := false;
  signal res_n    : std_logic;

  signal frame_sync    : std_logic;
  signal player1_input : snes_buttons_t;
  signal player2_input : snes_buttons_t;
begin
	tb : pong port map (
		clk => clk,
		res_n => res_n,

		--interface to graphics controller
		instr => open,
		instr_wr => open,
		instr_full => '0',
		frame_sync => frame_sync,

		--color selector
		drawing_color => (others => '1'),

		--game status information
		player1_points => open,
		player2_points => open,
		game_state => open,

		-- audio interface
		synth_cntrl => open,

		-- snes controller interface
		player1_input => player1_input,
		player2_input => player2_input
	);

  stimulus : process
	begin
    frame_sync <= '0';
    player1_input <= SNES_BUTTONS_RESET_VALUE;
    player2_input <= SNES_BUTTONS_RESET_VALUE;
    res_n <= '0';
    wait for 100 ns;
    res_n <= '1';

    --perform frame sync
    wait for 10 ms;
    wait until rising_edge(clk);
    frame_sync <= '1';
    wait until rising_edge(clk);
    frame_sync <= '0';

    player1_input.btn_start <= '1';

    --perform frame sync
    wait for 10 ms;
    wait until rising_edge(clk);
    frame_sync <= '1';
    wait until rising_edge(clk);
    frame_sync <= '0';
    player1_input.btn_start <= '0';

    --perform frame sync
    wait for 10 ms;
    wait until rising_edge(clk);
    frame_sync <= '1';
    wait until rising_edge(clk);
    frame_sync <= '0';

    --perform frame sync
    wait for 10 ms;
    wait until rising_edge(clk);
    frame_sync <= '1';
    wait until rising_edge(clk);
    frame_sync <= '0';

    --perform frame sync
    wait for 10 ms;
    wait until rising_edge(clk);
    frame_sync <= '1';
    wait until rising_edge(clk);
    frame_sync <= '0';

    --perform frame sync
    wait for 10 ms;
    wait until rising_edge(clk);
    frame_sync <= '1';
    wait until rising_edge(clk);
    frame_sync <= '0';

    wait;
  end process;

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
