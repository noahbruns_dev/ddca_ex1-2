
library ieee;
use ieee.std_logic_1164.all;

use work.graphics_controller_pkg.all;
use work.audio_cntrl_pkg.all;
use work.snes_cntrl_pkg.all;

package pong_pkg is

	type pong_game_state_t is (IDLE,RUNNING,PAUSED,PLAYER1_WON,PLAYER2_WON);

	component pong is
		port (
			clk : in std_logic;
			res_n : in std_logic;
			instr : out std_logic_vector(GCNTL_INSTR_WIDTH-1 downto 0);
			instr_wr : out std_logic;
			instr_full : in std_logic;
			frame_sync : in std_logic;
			drawing_color : in std_logic_vector(5 downto 0);
			player1_points : out std_logic_vector(5 downto 0);
			player2_points : out std_logic_vector(5 downto 0);
			game_state : out pong_game_state_t;
			synth_cntrl : out synth_cntrl_vec_t(0 to 1);
			player1_input : in snes_buttons_t;
			player2_input : in snes_buttons_t
		);
	end component;
end package;
