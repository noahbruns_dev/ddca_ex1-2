
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity pong_ball is
	generic(
		COORDINATE_WIDTH : integer := 12;
		DISPLAY_WIDTH    : integer := 800;
		DISPLAY_HEIGHT   : integer := 480;
		BALL_DIAMETER    : integer := 7;
		PLAYER_X_OFFSET : integer := 20;
		PLAYER_HEIGHT   : integer := 50
	);
	port (
		clk   : in std_logic;
		res_n : in std_logic;
		
		--control signals
		move_ball               : in std_logic;
		enable_player_collision : in std_logic;
		done                    : out std_logic;
		
		--coordinates 
		ball_x_in : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		ball_y_in : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		
		ball_x_out : out std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		ball_y_out : out std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		
		player1_y  : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		player2_y  : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		
		ball_speed_x : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
		ball_speed_y : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);

		--collision information
		collision_left        : out std_logic;
		collision_right       : out std_logic;
		collision_top         : out std_logic;
		collision_bottom      : out std_logic;
		collision_player1     : out std_logic;
		collision_player2     : out std_logic;
		collision_player_info : out std_logic_vector(COORDINATE_WIDTH-1 downto 0)
	);
end entity;

architecture arch of pong_ball is 
	type state_t is (IDLE, INIT, STEP, CHECK, READY);
	signal state : state_t;

	signal speed_x_sign, speed_y_sign : std_logic;
	signal bh_error : integer := 0;
	signal bh_dx : integer := 0;
	signal bh_dy : integer := 0;
begin
	sync : process(clk, res_n)
		variable err_temp_var : integer;
	begin
		if (res_n = '0') then
			state <= IDLE;
			ball_x_out  <= (others=>'0');
			ball_y_out  <= (others=>'0');
			bh_error    <= 0;
			done <= '0';
			collision_top <= '0';
			collision_bottom <= '0';
			collision_left <= '0';
			collision_right <= '0';
			collision_player1 <= '0';
			collision_player2 <= '0';
			collision_player_info <= (others=>'0');
		elsif (rising_edge(clk)) then
			case state is
				when IDLE =>
					done <= '0';
					collision_top <= '0';
					collision_bottom <= '0';
					collision_left <= '0';
					collision_right <= '0';
					collision_player1 <= '0';
					collision_player2 <= '0';
					if (move_ball='1') then
						if(unsigned(ball_speed_x) = 0 and unsigned(ball_speed_y)=0) then
							state <= READY; --nothing to do
							ball_x_out <= ball_x_in;
							ball_y_out <= ball_y_in;
						else
							state <= INIT;
						end if;
					end if;
					
				when INIT =>
					ball_x_out <= ball_x_in;
					ball_y_out <= ball_y_in;
					
					bh_dx <= abs(to_integer(signed(ball_speed_x)));
					bh_dy <= -abs(to_integer(signed(ball_speed_y)));
					
					bh_error <= abs(to_integer(signed(ball_speed_x)))-abs(to_integer(signed(ball_speed_y)));
					
					state <= STEP;

				when STEP =>
					err_temp_var := bh_error;
					if ( (bh_error*2) > bh_dy ) then 
						err_temp_var := err_temp_var + bh_dy;
						if(speed_x_sign='1') then
							ball_x_out <= std_logic_vector(unsigned(ball_x_out) - 1); 
						else
							ball_x_out <= std_logic_vector(unsigned(ball_x_out) + 1);
						end if;
					end if; 
				
					if ( (bh_error*2) < bh_dx ) then 
						err_temp_var := err_temp_var + bh_dx;
						if(speed_y_sign='1') then
							ball_y_out <= std_logic_vector(unsigned(ball_y_out) - 1); 
						else
							ball_y_out <= std_logic_vector(unsigned(ball_y_out) + 1);
						end if;
					end if;
					
					bh_error <= err_temp_var;
					state <= CHECK;
					
				when CHECK =>
					state <= STEP;
					
					if (unsigned(ball_y_out) = BALL_DIAMETER/2) then
						collision_top <= '1';
						state <= READY;
						done <= '1';
						ball_y_out <= std_logic_vector(unsigned(ball_y_out)+1);
					elsif (unsigned(ball_x_out) = BALL_DIAMETER/2) then
						collision_left <= '1';
						state <= READY;
						done <= '1';
						ball_x_out <= std_logic_vector(unsigned(ball_x_out)+1);
					end if;
					
					if (unsigned(ball_y_out) = DISPLAY_HEIGHT-1-BALL_DIAMETER/2) then
						collision_bottom <= '1';
						state <= READY;
						done <= '1';
						ball_y_out <= std_logic_vector(unsigned(ball_y_out)-1);
					elsif (unsigned(ball_x_out) = DISPLAY_WIDTH-1-BALL_DIAMETER/2) then
						collision_right <= '1';
						state <= READY;
						done <= '1';
						ball_x_out <= std_logic_vector(unsigned(ball_x_out)-1);
					end if;

					if(enable_player_collision = '1') then

						if (unsigned(ball_x_out) = PLAYER_X_OFFSET+BALL_DIAMETER/2 and
						    unsigned(ball_y_out)+BALL_DIAMETER/2 >= unsigned(player1_y) and
						    unsigned(ball_y_out)-BALL_DIAMETER/2 <= unsigned(player1_y)+PLAYER_HEIGHT-1 ) then 
							collision_player1 <= '1';
							collision_player_info <= std_logic_vector(signed(ball_y_out)-(signed(player1_y)+PLAYER_HEIGHT/2));
							state <= READY;
							done <= '1';
							ball_x_out <= std_logic_vector(unsigned(ball_x_out)+1);
						end if;
						

						if (unsigned(ball_x_out) = DISPLAY_WIDTH-PLAYER_X_OFFSET-BALL_DIAMETER/2 and
						    unsigned(ball_y_out)+BALL_DIAMETER/2 >= unsigned(player2_y) and
						    unsigned(ball_y_out)-BALL_DIAMETER/2 <= unsigned(player2_y)+PLAYER_HEIGHT-1 ) then 
							collision_player2 <= '1';
							collision_player_info <= std_logic_vector(signed(ball_y_out)-(signed(player2_y)+PLAYER_HEIGHT/2));
							state <= READY;
							done <= '1';
							ball_x_out <= std_logic_vector(unsigned(ball_x_out)-1);
						end if;
					end if;
					
					--check if we are done (no collision)
					if (unsigned(ball_x_out) = unsigned(ball_x_in)+unsigned(ball_speed_x) and
					    unsigned(ball_y_out) = unsigned(ball_y_in)+unsigned(ball_speed_y) ) then
						state <= READY;
						done <= '1';
					end if;
				
				when READY =>
					done <= '1';
					if(move_ball='0') then
						state <= IDLE;
					end if;
			end case;
		end if;
	end process;
	
	
	check_speed_direction : process(all)
	begin
		speed_x_sign <= '0';
		speed_y_sign <= '0';
		
		if(signed(ball_speed_x) < 0) then
			speed_x_sign <= '1';
		end if;
		
		if(signed(ball_speed_y) < 0) then
			speed_y_sign <= '1';
		end if;
	end process;


	

end architecture;
