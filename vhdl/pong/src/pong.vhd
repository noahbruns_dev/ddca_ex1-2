
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snes_cntrl_pkg.all;
use work.audio_cntrl_pkg.all;
use work.graphics_controller_pkg.all;
use work.pong_ball_pkg.all;
use work.pong_pkg.all;

entity pong is
	port (
		clk  : in std_logic;
		res_n : in std_logic;

		--interface to graphics controller
		instr      : out std_logic_vector(GCNTL_INSTR_WIDTH-1 downto 0);
		instr_wr   : out std_logic;
		instr_full : in std_logic;
		frame_sync    : in std_logic;

		--color selector
		drawing_color : in std_logic_vector(5 downto 0);

		--game status information
		player1_points : out std_logic_vector(5 downto 0);
		player2_points : out std_logic_vector(5 downto 0);
		game_state : out pong_game_state_t;

		-- audio interface
		synth_cntrl : out synth_cntrl_vec_t(0 to 1);

		-- snes controller interface
		player1_input : in snes_buttons_t;
		player2_input : in snes_buttons_t
	);
end entity;


architecture arch of pong is
	type pong_state_t is (RESET, INIT, INIT_FRAME_SYNC, INIT_2, SETUP_BLACK, SETUP_COLOR, PERFORM_FRAME_SYNC, WAIT_FRAME_SYNC, CLEAR_SCREEN, PROCESS_INPUT, CALC_GAME_STEP, CALC_GAME_STEP2, DRAW_BALL, DRAW_PADDLE_1, DRAW_PADDLE_2, DRAW_DASH);
	signal pong_state, pong_state_nxt : pong_state_t;

	constant COORDINATE_WIDTH : integer := 12;
	constant PLAYER_X_OFFSET : integer := 100;
	constant PLAYER_HEIGHT   : integer := 70;
	constant PLAYER_WIDTH   	: integer := 8;
	constant PLAYER_SPEED  	: integer := 8;
	constant INITIAL_SPEED_X   : integer := 6;
	constant INITIAL_SPEED_Y   : integer := 6;
	constant POINTS_TO_WIN   : integer := 3;
	constant MIDDLE_LINE_DASH_LENGTH : integer := 16;

	constant PLAYER_PADDLE_SPEED  	: unsigned := to_unsigned(PLAYER_SPEED, COORDINATE_WIDTH);
	constant PLAYER_PADDLE_MAX_Y  	: unsigned := to_unsigned(DISPLAY_HEIGHT-PLAYER_HEIGHT-PLAYER_SPEED, COORDINATE_WIDTH);
	constant PLAYER1_PADDLE_X  : signed := to_signed(PLAYER_X_OFFSET,COORDINATE_WIDTH);
	constant PLAYER2_PADDLE_X  : signed := to_signed(DISPLAY_WIDTH - PLAYER_X_OFFSET,COORDINATE_WIDTH);

	constant DASH_HEIGHT  : unsigned := to_unsigned(MIDDLE_LINE_DASH_LENGTH / 2, COORDINATE_WIDTH);
	constant DASH_DISTANCE  : unsigned := to_unsigned(MIDDLE_LINE_DASH_LENGTH, COORDINATE_WIDTH);

	signal ball_x, ball_x_nxt : std_logic_vector(COORDINATE_WIDTH-1 downto 0);
	signal ball_y, ball_y_nxt : std_logic_vector(COORDINATE_WIDTH-1 downto 0);

	signal start_btn_up, start_btn_up_nxt : std_logic;
	signal move_ball : std_logic;

	signal synth_cntrl_nxt : synth_cntrl_vec_t(0 to 1);

	signal ball_x_out : std_logic_vector(COORDINATE_WIDTH-1 downto 0);
	signal ball_y_out : std_logic_vector(COORDINATE_WIDTH-1 downto 0);

	signal ball_speed_x, ball_speed_x_nxt : std_logic_vector(COORDINATE_WIDTH-1 downto 0) := std_logic_vector(to_signed(INITIAL_SPEED_X, COORDINATE_WIDTH));
	signal ball_speed_y, ball_speed_y_nxt : std_logic_vector(COORDINATE_WIDTH-1 downto 0) := std_logic_vector(to_signed(INITIAL_SPEED_Y, COORDINATE_WIDTH));

	signal player1_y, player1_y_nxt, player2_y, player2_y_nxt : std_logic_vector(COORDINATE_WIDTH-1 downto 0) := std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));
	signal player_pause, player_pause_nxt : std_logic; --Player1: '0', Player2: '1'

	signal enable_player_collision, enable_player_collision_nxt : std_logic;
	signal clear_dashes, clear_dashes_nxt : std_logic;

	signal done              : std_logic;
	signal collision_left    : std_logic;
	signal collision_right   : std_logic;
	signal collision_top     : std_logic;
	signal collision_bottom  : std_logic;
	signal collision_player1 : std_logic;
	signal collision_player2 : std_logic;
	signal collision_player_info : std_logic_vector(COORDINATE_WIDTH-1 downto 0);

	signal dash_y, dash_y_nxt: std_logic_vector(COORDINATE_WIDTH-1 downto 0);

	signal player1_points_nxt : std_logic_vector(5 downto 0);
	signal player2_points_nxt : std_logic_vector(5 downto 0);

	signal game_state_nxt : pong_game_state_t;

	constant BALL_X_CENTER  : std_logic_vector(ball_x'range) := std_logic_vector(to_signed(DISPLAY_WIDTH/2-1,ball_speed_x'length));
	constant BALL_Y_CENTER  : std_logic_vector(ball_x'range) := std_logic_vector(to_signed(DISPLAY_HEIGHT/2-1,ball_speed_x'length));

	--clear_buffers
	type clear_buffer_t is record
    active : std_logic;
    instr : std_logic_vector(GCNTL_INSTR_WIDTH-1 downto 0);
	end record;
	type clear_buffer_a is array (0 to 40) of clear_buffer_t;
	constant clear_buffer_reset : clear_buffer_t := (active => '0', instr => (others =>'0'));

	signal clear_buffer, clear_buffer_nxt, clear_buffer_prev, clear_buffer_prev_nxt : clear_buffer_a;
	signal clear_buffer_count, clear_buffer_count_nxt : integer range 0 to 41;

	-- temporary counter to increment/decrement the points,
	-- this will not be needed in the final/full version of the game
	signal tmp_counter, tmp_counter_nxt : std_logic_vector(4 downto 0);

	--Audio
	type audio_state_t is (PAUSE, EDGE, PADDLE, POINT, GAME_END);
	signal audio_state, audio_state_nxt : audio_state_t;
	signal audio_state_prev, audio_state_prev_nxt : audio_state_t;
	signal audio_counter, audio_counter_nxt : integer range 0 to 50_000_000;
begin


	sync : process(clk,res_n)
	begin
		if (res_n = '0') then
			pong_state <= RESET;

			ball_x <= BALL_X_CENTER;
			ball_y <= BALL_Y_CENTER;
			ball_speed_x <= std_logic_vector(to_signed(INITIAL_SPEED_X, COORDINATE_WIDTH));
			ball_speed_y <= std_logic_vector(to_signed(INITIAL_SPEED_Y, COORDINATE_WIDTH));

			player1_y <= std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));
			player2_y <= std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));

			game_state <= IDLE;
			player1_points <= (others=>'0');
			player2_points <= (others=>'0');
			start_btn_up <= '0';
			clear_dashes <= '0';

			audio_state_prev <= PAUSE;

			clear_buffer <= (others => clear_buffer_reset);
			clear_buffer_prev <= (others => clear_buffer_reset);
			clear_buffer_count <= 0;

			tmp_counter <= (others=>'0');
		elsif (rising_edge(clk)) then
			pong_state <= pong_state_nxt;
			game_state <= game_state_nxt;
			clear_dashes <= clear_dashes_nxt;

			ball_x <= ball_x_nxt;
			ball_y <= ball_y_nxt;

			ball_speed_x <= ball_speed_x_nxt;
			ball_speed_y <= ball_speed_y_nxt;

			player1_points <= player1_points_nxt;
			player2_points <= player2_points_nxt;

			dash_y <= dash_y_nxt;

			enable_player_collision <= enable_player_collision_nxt;
			player_pause <= player_pause_nxt;

			player1_y <= player1_y_nxt;
			player2_y <= player2_y_nxt;
			start_btn_up <= start_btn_up_nxt;

			clear_buffer <= clear_buffer_nxt;
			clear_buffer_count <= clear_buffer_count_nxt;
			clear_buffer_prev <= clear_buffer_prev_nxt;

			audio_state_prev <= audio_state_prev_nxt;

			tmp_counter <= tmp_counter_nxt;
		end if;
	end process;

	next_state : process(all)
	begin
		pong_state_nxt <= pong_state;
		game_state_nxt <= game_state;

		instr <= (others=>'0');
		instr_wr <= '0';
		ball_x_nxt <= ball_x;
		ball_y_nxt <= ball_y;
		ball_speed_x_nxt <= ball_speed_x;
		ball_speed_y_nxt <= ball_speed_y;
		move_ball <= '0';

		player1_points_nxt <= player1_points;
		player2_points_nxt <= player2_points;

		player1_y_nxt <= player1_y;
		player2_y_nxt <= player2_y;

		dash_y_nxt <= dash_y;

		enable_player_collision_nxt <= enable_player_collision;
		player_pause_nxt <= player_pause;

		start_btn_up_nxt <= start_btn_up;
		clear_dashes_nxt <= clear_dashes;

		tmp_counter_nxt <= tmp_counter;

		clear_buffer_nxt <= clear_buffer;
		clear_buffer_prev_nxt <= clear_buffer_prev;
		clear_buffer_count_nxt <= clear_buffer_count;

		audio_state_prev_nxt <= PAUSE;

		case pong_state is
			when RESET =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_DOUBLE_BUFFERING_CFG;
					instr(8) <= '1';
					instr_wr <= '1';
					pong_state_nxt <= INIT;
				end if;

			when INIT =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_CLEAR_SCREEN;
					instr_wr <= '1';
					pong_state_nxt <= INIT_FRAME_SYNC;
				end if;

			when INIT_FRAME_SYNC =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_FRAME_SYNC;
					instr_wr <= '1';
					pong_state_nxt <= INIT_2;
				end if;

			when INIT_2 =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_CLEAR_SCREEN;
					instr_wr <= '1';
					pong_state_nxt <= PERFORM_FRAME_SYNC;
				end if;

			when PERFORM_FRAME_SYNC =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_FRAME_SYNC;
					instr_wr <= '1';
					pong_state_nxt <= WAIT_FRAME_SYNC;
					--resets
					clear_buffer_count_nxt <= 0;
					dash_y_nxt <= (others => '0');
				end if;

			when WAIT_FRAME_SYNC =>
				if (frame_sync = '1') then
					pong_state_nxt <= SETUP_BLACK;
				end if;

			when SETUP_BLACK =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_CHANGE_COLOR;
					instr(GCNTL_INSTR_WIDTH-1 downto 8) <= (others=>'1');

					instr(23 downto 8) <= (others=>'0');

					instr_wr <= '1';
					pong_state_nxt <= CLEAR_SCREEN;
				end if;

			when CLEAR_SCREEN =>
				if (instr_full = '0') then
					if (clear_buffer_prev(clear_buffer_count).active = '1') then
						instr <= clear_buffer_prev(clear_buffer_count).instr;
						instr_wr <= '1';
						clear_buffer_count_nxt <= clear_buffer_count + 1;
					else
						clear_buffer_prev_nxt <= clear_buffer;
						clear_buffer_nxt <= (others => clear_buffer_reset);
						pong_state_nxt <= SETUP_COLOR;
						clear_buffer_count_nxt <= 0;
					end if;
				end if;

			when SETUP_COLOR =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_CHANGE_COLOR;
					instr(GCNTL_INSTR_WIDTH-1 downto 8) <= (others=>'1');

					instr(23 downto 22) <= (others=>drawing_color(5));
					instr(21 downto 18) <= (others=>drawing_color(4));

					instr(18 downto 17) <= (others=>drawing_color(3));
					instr(16 downto 13) <= (others=>drawing_color(2));

					instr(12 downto 11) <= (others=>drawing_color(1));
					instr(10 downto 8)  <= (others=>drawing_color(0));

					instr_wr <= '1';
					pong_state_nxt <= PROCESS_INPUT;
				end if;

			when PROCESS_INPUT =>
				case game_state is
					when RUNNING =>
						enable_player_collision_nxt <= '1';

						if(player1_input.btn_start) then
							if (start_btn_up = '0') then
								game_state_nxt <= PAUSED;
								player_pause_nxt <= '0';
								start_btn_up_nxt <= '1';
							end if;
						elsif(player2_input.btn_start) then
							if (start_btn_up = '0') then
								game_state_nxt <= PAUSED;
								player_pause_nxt <= '1';
								start_btn_up_nxt <= '1';
							end if;
						else
							start_btn_up_nxt <= '0';
						end if;

						if(player1_input.btn_up) then
							if(unsigned(player1_y) > PLAYER_PADDLE_SPEED) then
								player1_y_nxt <= std_logic_vector(unsigned(player1_y) - PLAYER_PADDLE_SPEED);
							end if;
						elsif(player1_input.btn_down) then
							if(unsigned(player1_y) < PLAYER_PADDLE_MAX_Y) then
								player1_y_nxt <= std_logic_vector(unsigned(player1_y) + PLAYER_PADDLE_SPEED);
							end if;
						end if;

						if(player2_input.btn_up) then
							if(unsigned(player2_y) > PLAYER_PADDLE_SPEED) then
								player2_y_nxt <= std_logic_vector(unsigned(player2_y) - PLAYER_PADDLE_SPEED);
							end if;
						elsif(player2_input.btn_down) then
							if(unsigned(player2_y) < PLAYER_PADDLE_MAX_Y) then
								player2_y_nxt <= std_logic_vector(unsigned(player2_y) + PLAYER_PADDLE_SPEED);
							end if;
						end if;
					when IDLE =>
						enable_player_collision_nxt <= '0';
						if(player1_input.btn_start or player2_input.btn_start) then
							if (start_btn_up = '0') then
								game_state_nxt <= RUNNING;
								ball_x_nxt <= BALL_X_CENTER;
								ball_y_nxt <= BALL_Y_CENTER;
								ball_speed_x_nxt <= std_logic_vector(to_signed(INITIAL_SPEED_X, COORDINATE_WIDTH));
								ball_speed_y_nxt <= (others => '0');
								start_btn_up_nxt <= '1';
							end if;
						else
							start_btn_up_nxt <= '0';
						end if;
					when PLAYER1_WON =>
						enable_player_collision_nxt <= '0';
						if(player1_input.btn_start or player2_input.btn_start) then
							if (start_btn_up = '0') then
								game_state_nxt <= IDLE;
								ball_speed_x_nxt <= std_logic_vector(to_signed(INITIAL_SPEED_X, COORDINATE_WIDTH));
								ball_speed_y_nxt <= std_logic_vector(to_signed(INITIAL_SPEED_Y, COORDINATE_WIDTH));
								player1_points_nxt <= (others => '0');
								player2_points_nxt <= (others => '0');
								start_btn_up_nxt <= '1';
							end if;
						else
							start_btn_up_nxt <= '0';
						end if;
					when PLAYER2_WON =>
						enable_player_collision_nxt <= '0';
						if(player1_input.btn_start or player2_input.btn_start) then
							if (start_btn_up = '0') then
								game_state_nxt <= IDLE;
								ball_speed_x_nxt <= std_logic_vector(to_signed(INITIAL_SPEED_X, COORDINATE_WIDTH));
								ball_speed_y_nxt <= std_logic_vector(to_signed(INITIAL_SPEED_Y, COORDINATE_WIDTH));
								player1_points_nxt <= (others => '0');
								player2_points_nxt <= (others => '0');
								start_btn_up_nxt <= '1';
							end if;
						else
							start_btn_up_nxt <= '0';
						end if;
					when PAUSED =>
						if((player1_input.btn_start = '1' and player_pause_nxt = '0') or (player2_input.btn_start = '1' and player_pause_nxt = '1')) then
							if (start_btn_up = '0') then
								game_state_nxt <= RUNNING;
								start_btn_up_nxt <= '1';
							end if;
						else
							start_btn_up_nxt <= '0';
						end if;
					when others =>
						null;
				end case;

				if (game_state = PAUSED or game_state = PLAYER2_WON or game_state = PLAYER1_WON) then
					pong_state_nxt <= DRAW_BALL; --skip ball move
				else
					pong_state_nxt <= CALC_GAME_STEP;
				end if;

			when CALC_GAME_STEP =>
				move_ball <= '1';
				if (done = '1') then
					pong_state_nxt <= CALC_GAME_STEP2;
				end if;


			when CALC_GAME_STEP2 =>
				--get the new coordinate after collision
				ball_x_nxt <= ball_x_out;
				ball_y_nxt <= ball_y_out;

				case game_state is
					when IDLE =>
						if (collision_left = '1' or collision_right = '1') then
							ball_speed_x_nxt <= std_logic_vector( -signed(ball_speed_x));
						elsif (collision_top = '1' or collision_bottom = '1') then
							ball_speed_y_nxt <= std_logic_vector( -signed(ball_speed_y));
						end if;
					when RUNNING =>
						if (collision_left = '1') then
							player2_points_nxt <= std_logic_vector(unsigned(player2_points) + 1);
							ball_x_nxt <= BALL_X_CENTER;
							ball_y_nxt <= BALL_Y_CENTER;
							ball_speed_x_nxt <= std_logic_vector(to_signed(INITIAL_SPEED_X, COORDINATE_WIDTH));
							ball_speed_y_nxt <= (others => '0');
							player1_y_nxt <= std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));
							player2_y_nxt <= std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));
							player_pause_nxt <= '0';


							if (unsigned(player2_points) + 1 < POINTS_TO_WIN) then
								audio_state_prev_nxt <= POINT;
								game_state_nxt <= PAUSED;
							else
								audio_state_prev_nxt <= GAME_END;
								game_state_nxt <= PLAYER2_WON;
							end if;
						elsif (collision_right = '1') then
							player1_points_nxt <= std_logic_vector(unsigned(player1_points) + 1);
							ball_x_nxt <= BALL_X_CENTER;
							ball_y_nxt <= BALL_Y_CENTER;
							ball_speed_x_nxt <= std_logic_vector(to_signed(-INITIAL_SPEED_X, COORDINATE_WIDTH));
							ball_speed_y_nxt <= (others => '0');
							player1_y_nxt <= std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));
							player2_y_nxt <= std_logic_vector(to_signed(DISPLAY_HEIGHT/2 - PLAYER_HEIGHT/2, COORDINATE_WIDTH));
							player_pause_nxt <= '1';

							if (unsigned(player1_points) + 1 < POINTS_TO_WIN) then
								audio_state_prev_nxt <= POINT;
								game_state_nxt <= PAUSED;
							else
								audio_state_prev_nxt <= GAME_END;
								game_state_nxt <= PLAYER1_WON;
							end if;
						elsif (collision_top = '1' or collision_bottom = '1') then
							audio_state_prev_nxt <= EDGE;
							ball_speed_y_nxt <= std_logic_vector( -signed(ball_speed_y));
						elsif (collision_player1 = '1' or collision_player2 = '1') then
							audio_state_prev_nxt <= PADDLE;
							ball_speed_x_nxt <= std_logic_vector( -signed(ball_speed_x));
							ball_speed_y_nxt <= std_logic_vector( signed(ball_speed_y) + signed(shift_right(signed(collision_player_info), 3)) + to_signed(1, ball_speed_y'length));
						end if;
					when others =>
						null;
				end case;


				pong_state_nxt <= DRAW_BALL;

			when DRAW_BALL =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_DRAW_CIRCLE;
					instr(ball_x'length+8-1 downto 8) <= ball_x;
					instr(ball_y'length+8+12-1 downto 8+12) <= ball_y;
					instr(4+8+24-1 downto 8+24) <= x"7";
					instr_wr <= '1';

					clear_buffer_nxt(clear_buffer_count).active <= '1';
					clear_buffer_nxt(clear_buffer_count).instr <= instr;
					clear_buffer_count_nxt <= clear_buffer_count + 1;

					if (game_state = RUNNING or game_state = PAUSED or game_state = PLAYER1_WON or game_state = PLAYER2_WON) then
						pong_state_nxt <= DRAW_PADDLE_1;
					else
						pong_state_nxt <= PERFORM_FRAME_SYNC;
					end if;
				end if;

			when DRAW_PADDLE_1 =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_DRAW_RECTANGLE;
					instr(19 downto 8) <= std_logic_vector(PLAYER1_PADDLE_X);
					instr(31 downto 20) <= std_logic_vector( signed(player1_y) );
					instr(43 downto 32) <= std_logic_vector(PLAYER1_PADDLE_X - PLAYER_WIDTH);
					instr(55 downto 44) <= std_logic_vector( signed(player1_y) + PLAYER_HEIGHT );
					instr_wr <= '1';

					clear_buffer_nxt(clear_buffer_count).active <= '1';
					clear_buffer_nxt(clear_buffer_count).instr <= instr;
					clear_buffer_count_nxt <= clear_buffer_count + 1;

					pong_state_nxt <= DRAW_PADDLE_2;
				end if;

			when DRAW_PADDLE_2 =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_DRAW_RECTANGLE;
					instr(19 downto 8) <= std_logic_vector(PLAYER2_PADDLE_X);
					instr(31 downto 20) <= std_logic_vector( signed(player2_y) );
					instr(43 downto 32) <= std_logic_vector(PLAYER2_PADDLE_X + PLAYER_WIDTH);
					instr(55 downto 44) <= std_logic_vector( signed(player2_y) + PLAYER_HEIGHT );
					instr_wr <= '1';

					clear_buffer_nxt(clear_buffer_count).active <= '1';
					clear_buffer_nxt(clear_buffer_count).instr <= instr;
					clear_buffer_count_nxt <= clear_buffer_count + 1;

					pong_state_nxt <= DRAW_DASH;
				end if;

			when DRAW_DASH =>
				if (instr_full = '0') then
					instr(7 downto 0) <= GCNTL_INSTR_DRAW_RECTANGLE;
					instr(19 downto 8) <= std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2 - 1, COORDINATE_WIDTH));
					instr(31 downto 20) <= dash_y;
					instr(43 downto 32) <= std_logic_vector(to_unsigned(DISPLAY_WIDTH / 2, COORDINATE_WIDTH));
					instr(55 downto 44) <= std_logic_vector( unsigned(dash_y) + DASH_HEIGHT );
					instr_wr <= '1';

					clear_buffer_nxt(clear_buffer_count).active <= '1';
					clear_buffer_nxt(clear_buffer_count).instr <= instr;
					clear_buffer_count_nxt <= clear_buffer_count + 1;

					dash_y_nxt <= std_logic_vector( unsigned(dash_y) + DASH_DISTANCE );

					if (unsigned(dash_y) + DASH_HEIGHT > to_unsigned(DISPLAY_HEIGHT, COORDINATE_WIDTH)) then
						pong_state_nxt <= PERFORM_FRAME_SYNC;
					end if;
				end if;
		end case;
	end process;

	audio_sync : process(clk,res_n)
	begin
		if (res_n = '0') then
			audio_state <= PAUSE;
			audio_counter <= 0;
		elsif (rising_edge(clk)) then
			audio_state <= audio_state_nxt;
			audio_counter <= audio_counter_nxt;
			synth_cntrl <= synth_cntrl_nxt;
		end if;
	end process;

	audio_nxt : process(all)
	begin
		audio_counter_nxt <= audio_counter;
		audio_state_nxt <= audio_state;

		synth_cntrl_nxt <= synth_cntrl;

		if (audio_state_prev /= PAUSE) then
			audio_state_nxt <= audio_state_prev;
			audio_counter_nxt <= 0;
			synth_cntrl_nxt(0).play <= '0';
			synth_cntrl_nxt(1).play <= '0';
		else
			case audio_state is
				when PAUSE =>
					audio_counter_nxt <= 0;
					synth_cntrl_nxt(0).play <= '0';
					synth_cntrl_nxt(1).play <= '0';

				when EDGE =>
					synth_cntrl_nxt(0).play <= '1';
					synth_cntrl_nxt(1).play <= '0';

					synth_cntrl_nxt(0).high_time <= std_logic_vector(to_signed(12, 8));
					synth_cntrl_nxt(0).low_time <= std_logic_vector(to_signed(12, 8));

					audio_counter_nxt <= audio_counter + 1;
					if (audio_counter > 10_000_000) then
						audio_state_nxt <= PAUSE;
						audio_counter_nxt <= 0;
					end if;

				when PADDLE =>
					synth_cntrl_nxt(0).play <= '0';
					synth_cntrl_nxt(1).play <= '1';

					synth_cntrl_nxt(1).high_time <= std_logic_vector(to_signed(6, 8));
					synth_cntrl_nxt(1).low_time <= std_logic_vector(to_signed(6, 8));

					audio_counter_nxt <= audio_counter + 1;
					if (audio_counter > 10_000_000) then
						audio_state_nxt <= PAUSE;
						audio_counter_nxt <= 0;
					end if;

				when POINT =>
					audio_counter_nxt <= audio_counter + 1;

					synth_cntrl_nxt(0).high_time <= std_logic_vector(to_signed(12, 8));
					synth_cntrl_nxt(0).low_time <= std_logic_vector(to_signed(12, 8));

					synth_cntrl_nxt(1).high_time <= std_logic_vector(to_signed(8, 8));
					synth_cntrl_nxt(1).low_time <= std_logic_vector(to_signed(8, 8));

					if (audio_counter > 25_000_000) then
						audio_state_nxt <= PAUSE;
						audio_counter_nxt <= 0;
					elsif (audio_counter > 12_500_000) then
						synth_cntrl_nxt(0).play <= '1';
						synth_cntrl_nxt(1).play <= '1';
					else
						synth_cntrl_nxt(0).play <= '1';
						synth_cntrl_nxt(1).play <= '0';
					end if;

				when GAME_END =>
					audio_counter_nxt <= audio_counter + 1;
					if (audio_counter > 37_500_000) then
						audio_state_nxt <= PAUSE;
						audio_counter_nxt <= 0;
					elsif (audio_counter > 25_000_000) then
						synth_cntrl_nxt(0).high_time <= std_logic_vector(to_signed(3, 8));
						synth_cntrl_nxt(0).low_time <= std_logic_vector(to_signed(3, 8));
						synth_cntrl_nxt(0).play <= '1';
						synth_cntrl_nxt(1).play <= '0';
					elsif (audio_counter > 12_500_000) then
						synth_cntrl_nxt(1).high_time <= std_logic_vector(to_signed(6, 8));
						synth_cntrl_nxt(1).low_time <= std_logic_vector(to_signed(6, 8));
						synth_cntrl_nxt(0).play <= '0';
						synth_cntrl_nxt(1).play <= '1';
					else
						synth_cntrl_nxt(0).high_time <= std_logic_vector(to_signed(12, 8));
						synth_cntrl_nxt(0).low_time <= std_logic_vector(to_signed(12, 8));
						synth_cntrl_nxt(0).play <= '1';
						synth_cntrl_nxt(1).play <= '0';
					end if;
			end case;
		end if;
	end process;

	pong_ball_inst : pong_ball
	generic map (
		COORDINATE_WIDTH => COORDINATE_WIDTH,
		BALL_DIAMETER    => 13,
		PLAYER_X_OFFSET  => PLAYER_X_OFFSET,
		PLAYER_HEIGHT    => PLAYER_HEIGHT
	)
	port map (
		clk                     => clk,
		res_n                   => res_n,
		move_ball               => move_ball,
		ball_x_in               => ball_x,
		ball_y_in               => ball_y,
		ball_x_out              => ball_x_out,
		ball_y_out              => ball_y_out,
		player1_y               => player1_y,
		player2_y               => player2_y,
		ball_speed_x            => ball_speed_x,
		ball_speed_y            => ball_speed_y,
		enable_player_collision => enable_player_collision,
		done                    => done,
		collision_left          => collision_left,
		collision_right         => collision_right,
		collision_top           => collision_top,
		collision_bottom        => collision_bottom,
		collision_player1       => collision_player1,
		collision_player2       => collision_player2,
		collision_player_info   => collision_player_info
	);


end architecture;
