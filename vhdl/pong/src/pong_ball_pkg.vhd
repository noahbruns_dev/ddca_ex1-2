library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pong_ball_pkg is

	component pong_ball is
		generic (
			COORDINATE_WIDTH : integer := 10;
			DISPLAY_WIDTH : integer := 800;
			DISPLAY_HEIGHT : integer := 480;
			BALL_DIAMETER : integer := 7;
			PLAYER_X_OFFSET : integer := 20;
			PLAYER_HEIGHT : integer := 50
		);
		port (
			clk          : in std_logic;
			res_n        : in std_logic;
			move_ball    : in std_logic;
			done : out std_logic;
			ball_x_in    : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			ball_y_in    : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			ball_x_out   : out std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			ball_y_out   : out std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			player1_y    : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			player2_y    : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			ball_speed_x : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			ball_speed_y : in std_logic_vector(COORDINATE_WIDTH-1 downto 0);
			enable_player_collision : in std_logic;
			collision_left          : out std_logic;
			collision_right         : out std_logic;
			collision_top           : out std_logic;
			collision_bottom        : out std_logic;
			collision_player1 : out std_logic;
			collision_player2 : out std_logic;
			collision_player_info : out std_logic_vector(COORDINATE_WIDTH-1 downto 0)
		);
	end component;
end package;

