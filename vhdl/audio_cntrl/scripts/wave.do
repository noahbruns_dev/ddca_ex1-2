onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /audio_cntrl_tb/top_inst/wm8731_xck
add wave -noupdate /audio_cntrl_tb/top_inst/wm8731_sdat
add wave -noupdate /audio_cntrl_tb/top_inst/wm8731_sclk
add wave -noupdate /audio_cntrl_tb/top_inst/wm8731_dacdat
add wave -noupdate /audio_cntrl_tb/top_inst/wm8731_daclrck
add wave -noupdate /audio_cntrl_tb/top_inst/wm8731_bclk
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/state
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/state_nxt
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/busy_cnt
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/busy_cnt_nxt
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/i2c_ena
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/i2c_rw
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/i2c_data_wr
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/i2c_addr
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/busy
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/busy_prev
add wave -noupdate /audio_cntrl_tb/top_inst/fsm_init_inst/busy_prev_nxt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1761132 ps} 0} {{Cursor 2} {0 ps} 0} {{Cursor 3} {39764316 ps} 0} {{Cursor 4} {142871787 ps} 0}
quietly wave cursor active 3
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {163053676 ps}
