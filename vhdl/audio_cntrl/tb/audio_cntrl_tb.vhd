library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.audio_cntrl_pkg.all;

entity audio_cntrl_tb is
end entity;

architecture beh of audio_cntrl_tb is
	constant CLK_PERIOD : time := 83.333 ns;
	signal clk      : std_logic;
	signal stop_clock : boolean := false;

	signal synth_cntrl : synth_cntrl_vec_t(0 to 1);
begin
	synth_cntrl(0).play <= '1';
	synth_cntrl(0).high_time <= std_logic_vector(to_signed(40, 8));
	synth_cntrl(0).low_time <= std_logic_vector(to_signed(40, 8));

	synth_cntrl(1).play <= '1';
	synth_cntrl(1).high_time <= std_logic_vector(to_signed(40, 8));
	synth_cntrl(1).low_time <= std_logic_vector(to_signed(40, 8));

	top_inst : audio_cntrl
	generic map (
		SYNTH_COUNT => 2
	)
	port map (
		clk => clk,
		res_n => '1',

		wm8731_xck => open,
		wm8731_sdat => open,
		wm8731_sclk => open,
		wm8731_dacdat  => open,
		wm8731_daclrck  => open,
		wm8731_bclk  => open,
		synth_cntrl  => synth_cntrl
	);

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
