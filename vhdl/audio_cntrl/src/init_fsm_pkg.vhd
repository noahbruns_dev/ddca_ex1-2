LIBRARY ieee;
USE ieee.std_logic_1164.all;

package init_fsm_pkg is
	component init_fsm is
		port (
			clk  : in std_logic;
			res_n : in std_logic;
			
			sda   : INOUT  STD_LOGIC;
			scl   : INOUT  STD_LOGIC;
			
			init_done   : out std_logic
		);
	end component;
end package;