library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.synthesizer_pkg.all;
use work.audio_cntrl_pkg.all;

entity synthesizer is
	generic (
		OUTPUT_BIT_SIZE : integer := 6
	);
	port (
		clk  : in std_logic;
		res_n : in std_logic;
		
		synth_cntrl  	: in synth_cntrl_t;
		mix_out 			: out std_logic_vector(OUTPUT_BIT_SIZE - 1 downto 0)
	);
end entity;

architecture arch of synthesizer is
	type state_t is (LOW, HIGH, PAUSE);
	
	signal high_time, high_time_nxt : unsigned(31 downto 0);
	signal low_time, low_time_nxt : unsigned(31 downto 0);
	
	signal state, state_nxt : state_t := PAUSE;
	signal count, count_nxt : unsigned(31 downto 0) := (others => '0');
begin
	sync : process(clk,res_n)
	begin
		if (res_n = '0') then
			state <= PAUSE;
			count <= (others => '0');
		elsif (rising_edge(clk)) then
			state <= state_nxt;
			count <= count_nxt;
			
			high_time <= high_time_nxt;
			low_time <= low_time_nxt;
		end if;
	end process;
	
	output : process(all)
	begin
		case state is 
			when LOW =>
				mix_out <= std_logic_vector(to_signed(-1, OUTPUT_BIT_SIZE));
			when HIGH =>
				mix_out <= std_logic_vector(to_signed(1, OUTPUT_BIT_SIZE));
			when PAUSE =>
				mix_out <= std_logic_vector(to_signed(0, OUTPUT_BIT_SIZE));
		end case;
	end process;
	
	nxt : process(all)
	begin
		state_nxt <= state;
		count_nxt <= count + 1;
		
		high_time_nxt <= high_time;
		low_time_nxt <= low_time;
		
		case state is 
			when LOW =>
				if (synth_cntrl.play = '0') then
					state_nxt <= PAUSE;
				elsif (count >= low_time - 1) then
					state_nxt <= HIGH;
					count_nxt <= (others => '0');
				end if;
				
			when HIGH =>
				if (synth_cntrl.play = '0') then
					state_nxt <= PAUSE;
				elsif (count >= high_time - 1) then
					state_nxt <= LOW;
					count_nxt <= (others => '0');
				end if;
				
			when PAUSE =>
				if (synth_cntrl.play = '1') then
					high_time_nxt <= resize(unsigned(synth_cntrl.high_time), 16) * 1500;
					low_time_nxt <= resize(unsigned(synth_cntrl.low_time), 16) * 1500;
					state_nxt <= HIGH;
					count_nxt <= (others => '0');
				end if;
		end case;
	end process;
end architecture;