library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.mixer_pkg.all;

entity mixer is
	generic(
		SYNTH_COUNT  : integer
	);
	port (
		clk  : in std_logic;
		res_n : in std_logic;

		mix_in 		: std_logic_vector_arr(0 to SYNTH_COUNT - 1);
		mix_out  	: out signed(15 downto 0)
	);
end entity;

architecture arch of mixer is
	constant multiplyer : integer := 32767 / SYNTH_COUNT;

	signal mix_out_nxt : signed(15 downto 0);

	signal count, count_nxt : integer range 0 to SYNTH_COUNT + 2;
	signal calc, calc_nxt : signed(15 downto 0);
begin
	sync : process(clk,res_n)
	begin
		if (res_n = '0') then
			mix_out <= (others => '0');
			count <= 0;
			calc <= to_signed(0, 16);
		elsif (rising_edge(clk)) then
			mix_out <= mix_out_nxt;
			count <= count_nxt;
			calc <= calc_nxt;
		end if;
	end process;

	nxt : process(all)
	begin
		mix_out_nxt <= mix_out;
		count_nxt <= count + 1;

		if (count < SYNTH_COUNT) then
			calc_nxt <= calc + to_signed(to_integer(signed(mix_in(count))) * multiplyer, 16);
		else
			mix_out_nxt <= calc;
			count_nxt <= 0;
			calc_nxt <= to_signed(0, 16);
		end if;
	end process;
end architecture;
