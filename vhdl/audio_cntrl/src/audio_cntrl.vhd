library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.audio_cntrl_pkg.all;
use work.synthesizer_pkg.all;
use work.mixer_pkg.all;
use work.init_fsm_pkg.all;

entity audio_cntrl is
	generic(
		SYNTH_COUNT  : integer := 3
	);
	port (
		clk  : in std_logic;
		res_n : in std_logic;

		wm8731_xck  	: out std_logic;
		wm8731_sdat  	: inout std_logic;
		wm8731_sclk  	: inout std_logic;
		wm8731_dacdat  : out std_logic;
		wm8731_daclrck : out std_logic;
		wm8731_bclk  	: out std_logic;
		synth_cntrl  	: in synth_cntrl_vec_t(0 to SYNTH_COUNT - 1)
	);
end entity;

architecture bev of audio_cntrl is
	signal count, count_nxt : integer range 0 to 750;
	signal wm8731_daclrck_nxt, wm8731_dacdat_nxt : std_logic := '0';

	signal mix : std_logic_vector_arr(0 to SYNTH_COUNT - 1);
	signal audio_mix : signed(15 downto 0);

	signal audio_out, audio_out_nxt : signed(15 downto 0) := (others => '0');

	signal fsm_init_done : std_logic;

	signal start, start_nxt : std_logic := '0';
begin
	wm8731_bclk <= clk;
	wm8731_xck <= clk;

	sync : process(clk,res_n)
	begin
		if (res_n = '0') then
			count <= 0;
			wm8731_daclrck <= '0';
			wm8731_dacdat <= '0';
			audio_out <= (others => '0');
			start <= '0';
		elsif (falling_edge(clk)) then
			count <= count_nxt;
			wm8731_daclrck <= wm8731_daclrck_nxt;
			wm8731_dacdat <= wm8731_dacdat_nxt;
			audio_out <= audio_out_nxt;
			start <= start_nxt;
		end if;
	end process;

	nxt : process(all)
	begin
		count_nxt <= count + 1;
		wm8731_daclrck_nxt <= wm8731_daclrck;
		wm8731_dacdat_nxt <= '0';
		audio_out_nxt <= audio_out;
		start_nxt <= start;

		if (start = '0') then
			if (fsm_init_done = '1') then
				start_nxt <= '1';
				wm8731_daclrck_nxt <= '0';
				count_nxt <= 0;
				audio_out_nxt <= audio_mix;
			else
				count_nxt <= 0;
			end if;
		elsif (start = '1') then
			if (count < 16) then
				wm8731_dacdat_nxt <= audio_out(15 - count);
			elsif (count >= 749) then
				count_nxt <= 0;
				wm8731_daclrck_nxt <= not wm8731_daclrck;
				audio_out_nxt <= audio_mix;
			end if;
		end if;
	end process;

	GEN_SYN:
   for I in 0 to SYNTH_COUNT - 1 generate
      SYNTH : synthesizer port map
			(
				clk => clk,
				res_n => res_n,

				synth_cntrl => synth_cntrl(I),
				mix_out => mix(I)
			);
   end generate;

	mixer_inst : mixer
	generic map (
		SYNTH_COUNT => SYNTH_COUNT
	) port map(
		clk => clk,
		res_n => res_n,

		mix_in => mix,
		mix_out => audio_mix
	);

	fsm_init_inst : init_fsm port map (
		clk => clk,
		res_n => res_n,

		sda => wm8731_sdat,
		scl => wm8731_sclk,

		init_done => fsm_init_done
	);
end architecture;
