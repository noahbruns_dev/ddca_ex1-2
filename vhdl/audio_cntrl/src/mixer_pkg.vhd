library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.audio_cntrl_pkg.all;

package mixer_pkg is
	type std_logic_vector_arr is array (natural range <>) of std_logic_vector(5 downto 0);

	component mixer is
		generic(
			SYNTH_COUNT  : integer := 2
		);
		port (
			clk  : in std_logic;
			res_n : in std_logic;
			
			mix_in 		: std_logic_vector_arr(0 to SYNTH_COUNT - 1);
			mix_out  	: out signed(15 downto 0)
		);
	end component;
end package;