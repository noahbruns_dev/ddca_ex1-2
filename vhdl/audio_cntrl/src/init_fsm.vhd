library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.i2c_master_pkg.all;

entity init_fsm is
	port (
		clk  : in std_logic;
		res_n : in std_logic;

		sda   : INOUT  STD_LOGIC;
		scl   : INOUT  STD_LOGIC;

		init_done   : out std_logic
	);
end entity;

architecture arch of init_fsm is
	constant I2C_ADDR_SLAVE : std_logic_vector(6 downto 0) := "0011010";

	type state_t is (WAIT_I2C, AC_DEACT, AAC, DPC, PDC, DAI, SC, AC_ACTIV, DONE);
	signal state, state_nxt: state_t;

	signal busy_cnt, busy_cnt_nxt : integer range 0 to 4;

	signal i2c_ena			: STD_LOGIC;
	signal i2c_rw 			: STD_LOGIC;
	signal i2c_data_wr	: STD_LOGIC_VECTOR(7 DOWNTO 0);
	signal i2c_addr   	: STD_LOGIC_VECTOR(6 DOWNTO 0);
	signal busy, busy_prev, busy_prev_nxt   	: STD_LOGIC;
begin
	i2c_inst: i2c_master
	generic map(
		input_clk => 12_000_000
	)
	port map (
		clk => clk,
		reset_n => res_n,       	--active low reset
		ena => i2c_ena,						--latch in command
		addr => i2c_addr,      	--address of target slave
		rw => i2c_rw,                	--'0' is write, '1' is read
		data_wr => i2c_data_wr,		 	--data to write to slave
		busy => busy,              --indicates transaction in progress
		data_rd => open,				--data read from slave
		ack_error => open,         --flag if improper acknowledge from slave
		sda => sda,                --serial data output of i2c bus
		scl => scl
	);

	sync : process(clk,res_n)
	begin
		if (res_n = '0') then
			state <= WAIT_I2C;
			busy_cnt <= 0;
		elsif (rising_edge(clk)) then
			state <= state_nxt;
			busy_cnt <= busy_cnt_nxt;
			busy_prev <= busy_prev_nxt;
		end if;
	end process;

	nxt : process(all)
	begin
		state_nxt <= state;
		busy_prev_nxt <= busy;
		busy_cnt_nxt <= busy_cnt;
		init_done <= '0';
		i2c_addr <= I2C_ADDR_SLAVE;
		i2c_rw <= '0';
		i2c_ena <= '0';
		i2c_data_wr <= (others => '0');

		if (busy_prev = '0' and busy = '1') then
			busy_cnt_nxt <= busy_cnt + 1;
		end if;

		case state is
			when WAIT_I2C =>
				busy_cnt_nxt <= 0;
				if (busy = '0') then
					state_nxt <= AC_DEACT;
				end if;

			when AC_DEACT => --Active Control DEACTIVE
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00010010";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00000000";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= AAC;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when AAC => --Analog Audio Control
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00001000";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00011010";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= DPC;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when DPC => --Digtal Path Control
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00001010";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00000000";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= PDC;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when PDC => --Power Down Control
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00001100";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00000000";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= DAI;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when DAI => --Digital Audio Interface
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00001110";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00000010";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= SC;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when SC => --Sampling Control
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00010000";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00001101";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= AC_ACTIV;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when AC_ACTIV => --Active Control ACTIVE
				case busy_cnt is
					when 0 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00010010";
					when 1 =>
						i2c_ena <= '1';
						i2c_data_wr <= "00000001";
					when 2 =>
						i2c_ena <= '0';
						if(busy = '0') then
							state_nxt <= DONE;
							busy_cnt_nxt <= 0;
						end if;
					when others => busy_cnt_nxt <= 2;
				end case;

			when DONE =>
				i2c_ena <= '0';
				init_done <= '1';
				i2c_data_wr <= (others => '0');
		end case;
	end process;
end architecture;
