library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.audio_cntrl_pkg.all;

package synthesizer_pkg is
	component synthesizer is
		generic (
			OUTPUT_BIT_SIZE : integer := 6
		);
		port (
			clk  : in std_logic;
			res_n : in std_logic;
			
			synth_cntrl  	: in synth_cntrl_t;
			mix_out 			: out std_logic_vector(OUTPUT_BIT_SIZE - 1 downto 0)
		);
	end component;
end package;