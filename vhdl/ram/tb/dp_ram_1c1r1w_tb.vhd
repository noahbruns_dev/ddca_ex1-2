library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity dp_ram_1c1r1w_tb is
end entity;

architecture bench of dp_ram_1c1r1w_tb is

	component dp_ram_1c1r1w is
		generic (
			ADDR_WIDTH : integer;
			DATA_WIDTH : integer
		);
		port (
			clk : in std_logic;
			rd1_addr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
			rd1_data : out std_logic_vector(DATA_WIDTH - 1 downto 0);
			rd1 : in std_logic;
			wr2_addr : in std_logic_vector(ADDR_WIDTH - 1 downto 0);
			wr2_data : in std_logic_vector(DATA_WIDTH - 1 downto 0);
			wr2 : in std_logic
		);
	end component;

	constant ADDR_WIDTH : integer := 4;
	constant DATA_WIDTH : integer := 4;
	signal clk : std_logic;
	signal rd1_addr : std_logic_vector(ADDR_WIDTH - 1 downto 0);
	signal rd1_data : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal rd1 : std_logic;
	signal wr2_addr : std_logic_vector(ADDR_WIDTH - 1 downto 0);
	signal wr2_data : std_logic_vector(DATA_WIDTH - 1 downto 0);
	signal wr2 : std_logic;

	constant CLK_PERIOD : time := 10 ns;
	signal stop_clock : boolean := false;
begin

	uut : dp_ram_1c1r1w
		generic map (
			ADDR_WIDTH => ADDR_WIDTH,
			DATA_WIDTH => DATA_WIDTH
		)
		port map (
			clk      => clk,
			rd1_addr => rd1_addr,
			rd1_data => rd1_data,
			rd1      => rd1,
			wr2_addr => wr2_addr,
			wr2_data => wr2_data,
			wr2      => wr2
		);

	stimulus : process
	begin
		rd1_addr <= (others=>'0');
		rd1 <= '0';
		wr2_addr <= (others=>'0');
		wr2_data <= (others=>'0');
		wr2 <= '0';
		wait until rising_edge(clk);
		wait until rising_edge(clk);
		
		--write data to memory
		wr2 <= '1';
		for i in 0 to 2**ADDR_WIDTH-1 loop
			wr2_addr <= std_logic_vector(to_unsigned(i,ADDR_WIDTH) + 1);
			wr2_data <= std_logic_vector(to_unsigned(i,DATA_WIDTH) + 1);
			wait until rising_edge(clk);
		end loop;
		wr2 <= '0';
		
		--read data from memory
		rd1 <= '1';
		wait until rising_edge(clk);
		for i in 0 to 2**ADDR_WIDTH-1 loop
			rd1_addr <= std_logic_vector(to_unsigned(i,ADDR_WIDTH) + 1);
			wait until rising_edge(clk);
			assert rd1_data = std_logic_vector(to_unsigned(i,DATA_WIDTH)) report "Read data (" & to_string(rd1_data) & ") does not match the data previously written" severity error;
			
		end loop ;
		rd1 <= '0';
		
		stop_clock <= true;
		wait;
	end process;

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;

