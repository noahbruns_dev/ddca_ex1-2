onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Generics
add wave -noupdate /dp_ram_1c1r1w_tb/ADDR_WIDTH
add wave -noupdate /dp_ram_1c1r1w_tb/CLK_PERIOD
add wave -noupdate /dp_ram_1c1r1w_tb/DATA_WIDTH
add wave -noupdate -divider Signals
add wave -noupdate /dp_ram_1c1r1w_tb/stop_clock
add wave -noupdate /dp_ram_1c1r1w_tb/clk
add wave -noupdate /dp_ram_1c1r1w_tb/rd1
add wave -noupdate /dp_ram_1c1r1w_tb/rd1_addr
add wave -noupdate /dp_ram_1c1r1w_tb/rd1_data
add wave -noupdate /dp_ram_1c1r1w_tb/wr2
add wave -noupdate /dp_ram_1c1r1w_tb/wr2_addr
add wave -noupdate /dp_ram_1c1r1w_tb/wr2_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {86 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {250 ns}
