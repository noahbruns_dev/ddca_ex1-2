#!/bin/python3

from pynput import keyboard
from pynput.keyboard import Key, KeyCode
import serial,time
import threading
from threading import Thread
import os
import signal
import sys
from docopt import docopt

controller_image = """\
       _____                       _____       
   ___/ [L] \_____________________/ [R] \__    
  /                                        \   
 /     [↑]                          [X]     \  
|                                            | 
| [←]       [→]   [SE]  [ST]   [Y]       [A] | 
|                                            | 
 \     [↓]    __________________    [B]     /  
  \__________/                  \__________/   
"""

key_map = {
	Key.up:"up",
	Key.down:"down",
	Key.right:"right",
	Key.left:"left",
	"1":"start",
	"2":"select",
	"q":"l",
	"e":"r",
	"w":"x",
	"a":"y",
	"s":"b",
	"d":"a"
}

class SNESController:
	def __init__(self):
		self.up = False
		self.down = False
		self.left = False
		self.right = False
		self.a = False
		self.b = False
		self.x = False
		self.y = False
		self.start = False
		self.select = False
		self.l = False
		self.r = False
		
	def UpdateState(self, state):
		self.b =      bool(state[0] >> 0 & 1)
		self.y =      bool(state[0] >> 1 & 1)
		self.select = bool(state[0] >> 2 & 1)
		self.start =  bool(state[0] >> 3 & 1)
		self.up =     bool(state[0] >> 4 & 1)
		self.down =   bool(state[0] >> 5 & 1)
		self.left =   bool(state[0] >> 6 & 1)
		self.right =  bool(state[0] >> 7 & 1)

		self.a = bool(state[1] >> 0 & 1)
		self.x = bool(state[1] >> 1 & 1)
		self.l = bool(state[1] >> 2 & 1)
		self.r = bool(state[1] >> 3 & 1)
	
	def GetState(self):
		byte_low  = (int(self.b) << 0) + (int(self.y) << 1) + (int(self.select) << 2) + (int(self.start) << 3) + (int(self.up) << 4) + (int(self.down) << 5) + (int(self.left) << 6) + (int(self.right) << 7) 
		byte_high = (int(self.a) << 0 ) + (int(self.x) << 1 ) + (int(self.l) << 2 ) + (int(self.r) << 3 )
		return bytes([byte_low, byte_high])
	
	def DrawASCIIArt(self):
		output = controller_image
		r = [ 
		(self.up, "↑"),
		(self.down, "↓"),
		(self.up, "↑"),
		(self.left, "←"),
		(self.right, "→"),
		(self.a, "A"),
		(self.b, "B"),
		(self.x, "X"),
		(self.y, "Y"),
		(self.start, "ST"),
		(self.select, "SE"),
		(self.l, "L"),
		(self.r, "R") ]
		for x in r:
			if(not x[0]):
				output = output.replace("["+x[1]+"]", " " + x[1] + " ")
				
	
		return output


class DisplayThread(threading.Thread):
	def __init__(self, input_controller, output_controller):
		self._stopevent = threading.Event()
		self.input_controller = input_controller
		self.output_controller = output_controller

		threading.Thread.__init__(self)

	def run(self):
		while(not self._stopevent.isSet()):
			os.system("clear")
			print("Input State:")
			print(self.input_controller.DrawASCIIArt())
			print("Output State:")
			print(self.output_controller.DrawASCIIArt())
			self._stopevent.wait(0.1)
			#time.sleep(0.1)

	def join(self, timeout=None):
		self._stopevent.set()
		threading.Thread.join(self, timeout)


class InputThread(threading.Thread):
	def __init__(self, controller):
		self._stopevent = threading.Event()
		self.controller = controller
		threading.Thread.__init__(self)
		self.listener = None

	def run(self):
		def on_press(key):
			for k in key_map.keys():
				if (key == k or (isinstance(key,KeyCode) and key.char == k)):
					self.controller.__dict__[key_map[k]] = True

		def on_release(key):
			for k in key_map.keys():
				if (key == k or (isinstance(key,KeyCode) and key.char == k)):
					self.controller.__dict__[key_map[k]] = False

		self.listener = keyboard.Listener( on_press=on_press, on_release=on_release)
		self.listener.start()
		self.listener.join()

	def join(self, timeout=None):
		if(self.listener != None):
			self.listener.stop()
			threading.Thread.join(self, timeout)
			self.listener = None

def signal_handler(sig, frame):
	d_thread.join()
	i_thread.join()
	sys.exit(0)


d_thread = None
i_thread = None

__doc__ = """
SNES Controller

Usage: 
	controller -b BAUD_RATE [-d DEVICE] [-e]

-b BAUD_RATE    The baudrate of the UART interface
-e              Echo mode. The scripts simply echos the state it received. No 
                user input possible.
-d DEVICE       The device to use for the UART connection [default: /dev/ttyS0]
"""

if __name__ == "__main__":

	arguments = docopt(__doc__, version='1.0')
	print(arguments)
	echo_mode = arguments["-e"]

	input_controller = SNESController()
	output_controller = SNESController()
	
	signal.signal(signal.SIGINT, signal_handler)

	d_thread = DisplayThread(input_controller, output_controller)
	i_thread = InputThread(output_controller)

	d_thread.start()
	if(not echo_mode):
		i_thread.start()

	uart = serial.Serial()
	uart.port = arguments["-d"]
	uart.baudrate = arguments["-b"]
	uart.bytesize = serial.EIGHTBITS #number of bits per bytes
	uart.parity = serial.PARITY_NONE #set parity check: no parity
	uart.stopbits = serial.STOPBITS_ONE #number of stop bits
	uart.timeout = 500       #non-block read
	uart.xonxoff = False     #disable software flow control
	uart.rtscts = False     #disable hardware (RTS/CTS) flow control
	uart.dsrdtr = False       #disable hardware (DSR/DTR) flow control

	uart.open()

	while (True):
		read_data = uart.read(1)
		if(read_data == b"\xff"): # are we in sync
			state = uart.read(2)
			input_controller.UpdateState(state)
			
			if(echo_mode):
				output_controller.UpdateState(state)
			uart.write( b"\xff" + output_controller.GetState())
					


