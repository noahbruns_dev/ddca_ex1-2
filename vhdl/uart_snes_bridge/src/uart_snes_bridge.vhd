
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snes_cntrl_pkg.all;
use work.math_pkg.all;

entity uart_snes_bridge is
	generic (
		TX_TIMEOUT : integer := 500_000
	);
	port (
		clk : in std_logic;
		res_n : in std_logic; 
		
		button_state_in  : in snes_buttons_t;
		button_state_out : out snes_buttons_t;
		
		tx_data  : out std_logic_vector(7 downto 0);
		tx_wr    : out std_logic;
		tx_free  : in std_logic;
		
		rx_data  : in std_logic_vector(7 downto 0);
		rx_rd    : out std_logic;
		rx_empty : in std_logic
	);
end entity;

architecture arch of uart_snes_bridge is
	constant SYNC_PATTERN : std_logic_vector(7 downto 0) := x"ff";
begin
	snes_to_uart : block 
		type tx_state_t is (READ_INPUT, SEND_SYNC_PATTERN, SEND_LOW_BYTE, SEND_HIGH_BYTE, TIMEOUT);
		signal tx_state : tx_state_t;
		signal timeout_cnt : std_logic_vector(log2c(TX_TIMEOUT+1)-1 downto 0);
		signal button_state : snes_buttons_t;
	begin
		
		sync : process(clk, res_n)
		begin
			if (res_n = '0') then
				tx_data <= (others=>'0');
				timeout_cnt <= (others=>'0');
				tx_wr <= '0';
				tx_state <= TIMEOUT;
				button_state <= (others=>'0');
			elsif (rising_edge(clk)) then
				tx_wr <= '0';
				case tx_state is
					when READ_INPUT =>
						button_state <= button_state_in;
						tx_state <= SEND_SYNC_PATTERN;
					
					when SEND_SYNC_PATTERN =>
						tx_data <= SYNC_PATTERN;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= SEND_LOW_BYTE;
						end if;
						
					when SEND_LOW_BYTE =>
						tx_data(0) <= button_state.btn_b;
						tx_data(1) <= button_state.btn_y;
						tx_data(2) <= button_state.btn_select;
						tx_data(3) <= button_state.btn_start;
						tx_data(4) <= button_state.btn_up;
						tx_data(5) <= button_state.btn_down;
						tx_data(6) <= button_state.btn_left;
						tx_data(7) <= button_state.btn_right;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= SEND_HIGH_BYTE;
						end if;
					
					when SEND_HIGH_BYTE =>
						tx_data <= (others=>'1');
						tx_data(0) <= button_state.btn_a;
						tx_data(1) <= button_state.btn_x;
						tx_data(2) <= button_state.btn_l;
						tx_data(3) <= button_state.btn_r;
						if (tx_free = '1') then
							tx_wr <= '1';
							tx_state <= TIMEOUT;
						end if;
						
					when TIMEOUT => 
						if (unsigned(timeout_cnt) = TX_TIMEOUT ) then
							timeout_cnt <= (others=>'0');
							tx_state <= READ_INPUT;
						else
							timeout_cnt <= std_logic_vector(unsigned(timeout_cnt)+1);
						end if;
				end case;
			end if;
		end process;
		
		
	end block;
	
	
	uart_to_snes : block
		type rx_state_t is (IDLE, CHECK_SYNC_PATTERN, READ_LOW_BYTE, READ_HIGH_BYTE);
		signal rx_state, rx_state_nxt : rx_state_t;
		signal low_byte, low_byte_nxt : std_logic_vector(7 downto 0);
		signal button_state_out_nxt : snes_buttons_t;
	begin
		sync : process(clk, res_n)
		begin
			if (res_n = '0') then
				rx_state <= IDLE;
				button_state_out <= (others=>'0');
				low_byte <= (others=>'0');
			elsif (rising_edge(clk)) then
				low_byte <= low_byte_nxt;
				rx_state <= rx_state_nxt;
				button_state_out <= button_state_out_nxt;
			end if;
		end process;
	
		next_state : process(all)
		begin
			low_byte_nxt <= low_byte;
			rx_state_nxt <= rx_state;
			button_state_out_nxt <= button_state_out;
		
			rx_rd <= '0';
			case rx_state is
				when IDLE =>
					if (rx_empty = '0') then
						rx_state_nxt <= CHECK_SYNC_PATTERN;
						rx_rd <= '1';
					end if;
				
				when CHECK_SYNC_PATTERN =>
					if(rx_data = SYNC_PATTERN) then
						if (rx_empty = '0') then
							rx_state_nxt <= READ_LOW_BYTE;
							rx_rd <= '1';
						end if;
					else 
						rx_state_nxt <= IDLE;
					end if;
					
				when READ_LOW_BYTE =>
					low_byte_nxt <= rx_data;
					if (rx_empty = '0') then
						rx_state_nxt <= READ_HIGH_BYTE;
						rx_rd <= '1';
					end if;
				
				when READ_HIGH_BYTE =>
					button_state_out_nxt.btn_b      <= low_byte(0);
					button_state_out_nxt.btn_y      <= low_byte(1);
					button_state_out_nxt.btn_select <= low_byte(2);
					button_state_out_nxt.btn_start  <= low_byte(3);
					button_state_out_nxt.btn_up     <= low_byte(4);
					button_state_out_nxt.btn_down   <= low_byte(5);
					button_state_out_nxt.btn_left   <= low_byte(6);
					button_state_out_nxt.btn_right  <= low_byte(7);
					
					button_state_out_nxt.btn_a      <= rx_data(0);
					button_state_out_nxt.btn_x      <= rx_data(1);
					button_state_out_nxt.btn_l      <= rx_data(2);
					button_state_out_nxt.btn_r      <= rx_data(3);
					rx_state_nxt <= IDLE;
			end case;
	
		end process;
	
	end block;
	
	
end architecture;

