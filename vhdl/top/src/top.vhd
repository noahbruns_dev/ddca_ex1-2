
--------------------------------------------------------------------------------
--                                LIBRARIES                                   --
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- import all required packages
use work.audio_cntrl_pkg.all;
use work.framereader_pkg.all;
use work.graphics_controller_pkg.all;
use work.math_pkg.all;
use work.pong_ball_pkg.all;
use work.pong_pkg.all;
use work.pong_ssd_pkg.all;
use work.ram_pkg.all;
use work.rasterizer_pkg.all;
use work.serial_port_pkg.all;
use work.snes_cntrl_pkg.all;
use work.sram_pkg.all;
use work.sync_pkg.all;
use work.uart_snes_bridge_pkg.all;
use work.wb_arbiter_pkg.all;

--------------------------------------------------------------------------------
--                                 ENTITY                                     --
--------------------------------------------------------------------------------

entity top is
	port (
		--50 MHz clock input
		clk      : in  std_logic;

		-- push buttons, switches and LEDs
		keys     : in std_logic_vector(3 downto 0);
		switches : in std_logic_vector(17 downto 0);
		ledg     : out std_logic_vector(8 downto 0);
		ledr     : out std_logic_vector(17 downto 0);
	
		--Seven segment digit
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0);
		hex4 : out std_logic_vector(6 downto 0);
		hex5 : out std_logic_vector(6 downto 0);
		hex6 : out std_logic_vector(6 downto 0);
		hex7 : out std_logic_vector(6 downto 0);

		-- external interface to SRAM
		sram_dq   : inout std_logic_vector(15 downto 0);
		sram_addr : out std_logic_vector(19 downto 0);
		sram_ub_n : out std_logic;
		sram_lb_n : out std_logic;
		sram_we_n : out std_logic;
		sram_ce_n : out std_logic;
		sram_oe_n : out std_logic;
		
		-- LCD interface
		nclk    : out std_logic;
		hd      : out std_logic;
		vd      : out std_logic;
		den     : out std_logic;
		r       : out std_logic_vector(7 downto 0);
		g       : out std_logic_vector(7 downto 0);
		b       : out std_logic_vector(7 downto 0);
		grest   : out std_logic;
		sda     : out std_logic;
		
		--interface to snes controller
		snes_clk   : out std_logic;
		snes_latch : out std_logic;
		snes_data  : in std_logic;

		--uart
		tx      : out std_logic;
		rx      : in std_logic;
		
		--Debug
		clk_db     : out std_logic;  --0
		hd_db      : out std_logic;  --15
		vd_db      : out std_logic;  --14
		den_db     : out std_logic;  --13
		r_db       : out std_logic;  --12
		g_db       : out std_logic;  --11
		b_db       : out std_logic;  --10
		snes_clk_db   : out std_logic;  --9
		snes_latch_db : out std_logic;  --8
		snes_data_db  : out std_logic;  --7
		tx_db      : out std_logic;  --6
		
		--Audio
		wm8731_xck  	: out std_logic;
		wm8731_sdat  	: inout std_logic;
		wm8731_sclk  	: inout std_logic;
		wm8731_dacdat  : out std_logic;
		wm8731_daclrck : out std_logic;
		wm8731_bclk  	: out std_logic
	);
end entity;


--------------------------------------------------------------------------------
--                             ARCHITECTU:RE                                   --
--------------------------------------------------------------------------------

-- add your architecture here!
ARCHITECTURE structural OF top IS
	constant SYS_CLK_FREQ 	: NATURAL := 50_000_000;
	constant TX_TIMEOUT_MS 	: NATURAL := 10;
	constant SYNC_STAGES 	: NATURAL := 2;
	
	signal synth_cntrl : synth_cntrl_vec_t(0 to 1);
	signal synth_cntrl_sync : synth_cntrl_vec_t(0 to 1);
	
	signal sys_clk, sys_res_n : std_logic;
	
	signal player1_button_state, player2_button_state : snes_buttons_t;
	
	signal instr      	: std_logic_vector(GCNTL_INSTR_WIDTH-1 downto 0);
	signal instr_wr   	: std_logic;
	signal instr_full 	: std_logic;
	signal frame_sync    : std_logic;
	
	signal display_clk 	: std_logic;
	signal display_res_n : std_logic;
	signal tx_data  : std_logic_vector(7 downto 0);
	signal tx_wr    : std_logic;
	signal tx_free  : std_logic;
	
	signal rx_data  : std_logic_vector(7 downto 0);
	signal rx_rd    : std_logic;
	signal rx_empty : std_logic;
	
	signal player1_points : std_logic_vector(5 downto 0);
	signal player2_points : std_logic_vector(5 downto 0);
	
	signal game_state : pong_game_state_t;
	
	signal audio_clk : std_logic;
	signal audio_res_n : std_logic;
	
	COMPONENT pll IS
	PORT
	(
		inclk0	: IN STD_LOGIC;
		c0			: OUT STD_LOGIC;
		c1			: OUT STD_LOGIC 
	);
	END COMPONENT;
	
BEGIN
	ledg <= (others => '0');
	ledr(17 downto 12) <= (others => '0');
	
	sys_clk <= clk;
	
	ledr(0) <= player2_button_state.btn_up;
	ledr(1) <= player2_button_state.btn_down;
	ledr(2) <= player2_button_state.btn_left;
	ledr(3) <= player2_button_state.btn_right;
	ledr(4) <= player2_button_state.btn_a;
	ledr(5) <= player2_button_state.btn_b;
	ledr(6) <= player2_button_state.btn_x;
	ledr(7) <= player2_button_state.btn_y;
	ledr(8) <= player2_button_state.btn_start;
	ledr(9) <= player2_button_state.btn_select;
	ledr(10) <= player2_button_state.btn_r;
	ledr(11) <= player2_button_state.btn_l;
	
	clk_db <= sys_clk;
	hd_db <= hd;
	vd_db <= vd;
	den_db <= den;
	r_db <= r(7);
	g_db <= g(7);
	b_db <= b(7);
	snes_clk_db <= snes_clk;
	snes_latch_db <= snes_latch;
	snes_data_db <= snes_data;
	tx_db <= tx;

	pll_inst : pll port map(
		inclk0 => sys_clk,
		c0 => display_clk,
		c1 => audio_clk
	);
	
	pong_inst: pong port map(
		clk => sys_clk,
		res_n => sys_res_n,
		
		instr	=> instr,
		instr_wr => instr_wr,
		instr_full => instr_full,
		frame_sync => frame_sync,
		
		drawing_color => switches(17 downto 12),
		
		player1_points => player1_points,
		player2_points => player2_points,
		game_state => game_state,
		
		synth_cntrl => synth_cntrl,
		
		player1_input => player1_button_state,
		player2_input => player2_button_state
	);
	
	gcntrl_inst: graphics_controller port map(
		clk => sys_clk,
		res_n => sys_res_n,
		display_clk => display_clk,
		display_res_n => display_res_n,
		instr => instr,
		instr_wr => instr_wr,
		instr_full => instr_full,
		current_color => open,
		frame_sync => frame_sync,
		sram_dq => sram_dq,
		sram_addr => sram_addr,
		sram_ub_n => sram_ub_n,
		sram_lb_n => sram_lb_n,
		sram_we_n => sram_we_n,
		sram_ce_n => sram_ce_n,
		sram_oe_n => sram_oe_n,
		nclk => nclk,
		hd => hd,
		vd => vd,
		den => den,
		r => r,
		g => g,
		b => b,
		grest => grest,
		sda => sda
	);
	
	sys_reset_sync: sync 
	generic map (
		SYNC_STAGES => SYNC_STAGES,
		RESET_VALUE => '1'
	)
	port map(
		clk => sys_clk,
		res_n => '1',
		data_in => keys(0),
		data_out => sys_res_n
	);
	
	display_reset_sync: sync 
	generic map (
		SYNC_STAGES => SYNC_STAGES,
		RESET_VALUE => '1'
	)
	port map(
		clk => display_clk,
		res_n => '1',
		data_in => keys(0),
		data_out => display_res_n
	);
	
	snes_cntrl_inst : snes_cntrl
	port map (
		clk => sys_clk,
		res_n => sys_res_n,
		snes_latch => snes_latch,
		snes_clk => snes_clk,
		snes_data => snes_data,
		button_state => player1_button_state
	);
	
	uart_snes_bridge_inst : uart_snes_bridge
	generic map(
		TX_TIMEOUT => TX_TIMEOUT_MS * SYS_CLK_FREQ / 1000
	)
	port map(
		clk => sys_clk,
		res_n => sys_res_n,
		
		button_state_in => player1_button_state,
		button_state_out => player2_button_state,
		
		tx_data => tx_data,
		tx_wr => tx_wr,
		tx_free => tx_free,
		
		rx_data => rx_data,
		rx_rd => rx_rd,
		rx_empty => rx_empty
	);
	
	serial_port_inst : serial_port port map(
		clk => sys_clk,
		res_n => sys_res_n,
		
		rx => rx,
		tx => tx,
		
		tx_data => tx_data,
		tx_wr => tx_wr,
		tx_free => tx_free,
		rx_rd => rx_rd,
		rx_data => rx_data,
		rx_empty => rx_empty
	);
	
	pong_ssd_inst : pong_ssd port map(
		game_state => game_state,
		clk => sys_clk,
		res_n => sys_res_n,
		
		player1_points => player1_points,
		player2_points => player2_points,
		
		hex0 => hex0,
		hex1 => hex1,
		hex2 => hex2,
		hex3 => hex3,
		hex4 => hex4,
		hex5 => hex5,
		hex6 => hex6,
		hex7 => hex7
	);
	
	audio_reset_sync: sync 
	generic map (
		SYNC_STAGES => SYNC_STAGES,
		RESET_VALUE => '1'
	)
	port map(
		clk => audio_clk,
		res_n => '1',
		data_in => sys_res_n,
		data_out => audio_res_n
	);
	
	play_sync_1: sync 
	generic map (
		SYNC_STAGES => SYNC_STAGES,
		RESET_VALUE => '1'
	)
	port map(
		clk => audio_clk,
		res_n => '1',
		data_in => synth_cntrl(0).play,
		data_out => synth_cntrl_sync(0).play
	);
	
	play_sync_2: sync 
	generic map (
		SYNC_STAGES => SYNC_STAGES,
		RESET_VALUE => '1'
	)
	port map(
		clk => audio_clk,
		res_n => '1',
		data_in => synth_cntrl(1).play,
		data_out => synth_cntrl_sync(1).play
	);
	
	synth_cntrl_sync(0).low_time <= synth_cntrl(0).low_time;
	synth_cntrl_sync(0).high_time <= synth_cntrl(0).high_time;
	
	synth_cntrl_sync(1).low_time <= synth_cntrl(1).low_time;
	synth_cntrl_sync(1).high_time <= synth_cntrl(1).high_time;
	
	top_inst : audio_cntrl
	generic map (
		SYNTH_COUNT => 2
	)
	port map (
		clk => audio_clk,
		res_n => audio_res_n,
		
		wm8731_xck => wm8731_xck,
		wm8731_sdat => wm8731_sdat,
		wm8731_sclk => wm8731_sclk,
		wm8731_dacdat  => wm8731_dacdat,
		wm8731_daclrck  => wm8731_daclrck,
		wm8731_bclk  => wm8731_bclk,
		synth_cntrl  => synth_cntrl_sync
	);
END structural;

