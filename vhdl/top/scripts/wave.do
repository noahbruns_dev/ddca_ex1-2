onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /top_tb/top_inst/pong_inst/clk
add wave -noupdate /top_tb/top_inst/pong_inst/res_n
add wave -noupdate /top_tb/top_inst/pong_inst/instr
add wave -noupdate /top_tb/top_inst/pong_inst/instr_wr
add wave -noupdate /top_tb/top_inst/pong_inst/instr_full
add wave -noupdate /top_tb/top_inst/pong_inst/frame_sync
add wave -noupdate /top_tb/top_inst/pong_inst/drawing_color
add wave -noupdate /top_tb/top_inst/pong_inst/player1_points
add wave -noupdate /top_tb/top_inst/pong_inst/player2_points
add wave -noupdate /top_tb/top_inst/pong_inst/game_state
add wave -noupdate /top_tb/top_inst/pong_inst/synth_cntrl
add wave -noupdate /top_tb/top_inst/pong_inst/player1_input
add wave -noupdate /top_tb/top_inst/pong_inst/player2_input
add wave -noupdate /top_tb/top_inst/pong_inst/pong_state
add wave -noupdate /top_tb/top_inst/pong_inst/pong_state_nxt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {99839500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {512616800 ps}
