library ieee;
use ieee.std_logic_1164.all;

entity top_tb is
end entity;


architecture beh of top_tb is

	component top is
		port (
			--50 MHz clock input
			clk      : in  std_logic;

			-- push buttons, switches and LEDs
			keys     : in std_logic_vector(3 downto 0);
			switches : in std_logic_vector(17 downto 0);
			ledg     : out std_logic_vector(8 downto 0);
			ledr     : out std_logic_vector(17 downto 0);

			--Seven segment digit
			hex0 : out std_logic_vector(6 downto 0);
			hex1 : out std_logic_vector(6 downto 0);
			hex2 : out std_logic_vector(6 downto 0);
			hex3 : out std_logic_vector(6 downto 0);
			hex4 : out std_logic_vector(6 downto 0);
			hex5 : out std_logic_vector(6 downto 0);
			hex6 : out std_logic_vector(6 downto 0);
			hex7 : out std_logic_vector(6 downto 0);

			-- external interface to SRAM
			sram_dq   : inout std_logic_vector(15 downto 0);
			sram_addr : out std_logic_vector(19 downto 0);
			sram_ub_n : out std_logic;
			sram_lb_n : out std_logic;
			sram_we_n : out std_logic;
			sram_ce_n : out std_logic;
			sram_oe_n : out std_logic;

			-- LCD interface
			nclk    : out std_logic;
			hd      : out std_logic;
			vd      : out std_logic;
			den     : out std_logic;
			r       : out std_logic_vector(7 downto 0);
			g       : out std_logic_vector(7 downto 0);
			b       : out std_logic_vector(7 downto 0);
			grest   : out std_logic;
			sda     : out std_logic;

			--interface to snes controller
			snes_clk   : out std_logic;
			snes_latch : out std_logic;
			snes_data  : in std_logic;

			--uart
			tx      : out std_logic;
			rx      : in std_logic
		);
	end component;

	constant CLK_PERIOD : time := 20 ns;

	signal clk      : std_logic;
	signal switches : std_logic_vector(17 downto 0);
	signal keys : std_logic_vector(3 downto 0);
	signal rx : std_logic;
	signal snes_data : std_logic;
	signal snes_latch : std_logic;
	signal snes_clk : std_logic;

	signal start : std_logic;

	signal stop_clock : boolean := false;
begin

	top_inst : top port map(
		clk => clk,
		keys => keys,
		switches => switches,
		rx => rx,
		snes_data => snes_data,
		snes_latch => snes_latch,
		snes_clk => snes_clk
	);
	-----------------------------------------------
	--   Instantiate your top level entity here  --
	-----------------------------------------------
	-- The testbench generates the above singals --
	-- use them as inputs to your system         --
	-----------------------------------------------

	stimulus : process
	begin
		keys <= (others=>'0');
		keys(0) <= '0';
		switches <= (others=>'0');
		rx <= '0';
		switches(17 downto 12) <= (others=>'1'); --set drawing color to white

		wait for CLK_PERIOD * 40;
		keys(0) <= '1'; -- release the reset

    wait for 50 ms;
    start <= '1';

    wait for 100 ms;
    start <= '0';

		wait;
	end process;

	stimulus_snes : process
	begin
		wait until rising_edge(snes_latch);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= start;
		wait until rising_edge(snes_clk);


		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
	end process;

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
