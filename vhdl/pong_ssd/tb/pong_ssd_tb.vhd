library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.pong_ssd_pkg.all;
use work.pong_pkg.all;

entity pong_ssd_tb is
end entity;


architecture beh of pong_ssd_tb is
	constant CLK_PERIOD : time := 20 ns;
	signal clk      : std_logic;
	signal stop_clock : boolean := false;
	signal game_state : pong_game_state_t;
	
	signal player1_points : std_logic_vector(5 downto 0) := (others => '0');
	signal player2_points : std_logic_vector(5 downto 0) := (others => '0');
begin

	game_state <= RUNNING;
	
	top_inst : pong_ssd
	port map(
		res_n => '1',
		game_state => game_state,
		clk => clk,
		player1_points => player1_points,
		player2_points => player2_points
	);
	-----------------------------------------------
	--   Instantiate your top level entity here  --
	-----------------------------------------------
	-- The testbench generates the above singals --
	-- use them as inputs to your system         --
	-----------------------------------------------
	
	player1_points <= std_logic_vector(to_unsigned(13, 6));
	player2_points <= std_logic_vector(to_unsigned(50, 6));

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
