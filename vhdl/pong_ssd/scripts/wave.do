onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /pong_ssd_tb/clk
add wave -noupdate /pong_ssd_tb/top_inst/player1_points
add wave -noupdate /pong_ssd_tb/top_inst/player2_points
add wave -noupdate /pong_ssd_tb/top_inst/hex4
add wave -noupdate /pong_ssd_tb/top_inst/hex5
add wave -noupdate /pong_ssd_tb/top_inst/p1_inst/state
add wave -noupdate /pong_ssd_tb/top_inst/p1_inst/next_state
add wave -noupdate -expand /pong_ssd_tb/top_inst/hex6
add wave -noupdate -expand /pong_ssd_tb/top_inst/hex7
TreeUpdate [SetDefaultTree]
WaveRestoreCursors
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {1995420 ps}
