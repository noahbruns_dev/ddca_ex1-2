library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pong_pkg.all;

package pong_ssd_pkg is
	component pong_ssd is
		port (
			game_state : in pong_game_state_t;
			clk : in std_logic;
			res_n : in std_logic;
			
			player1_points : in std_logic_vector(5 downto 0);
			player2_points : in std_logic_vector(5 downto 0);
			
			hex0 : out std_logic_vector(6 downto 0);
			hex1 : out std_logic_vector(6 downto 0);
			hex2 : out std_logic_vector(6 downto 0);
			hex3 : out std_logic_vector(6 downto 0);
			hex4 : out std_logic_vector(6 downto 0);
			hex5 : out std_logic_vector(6 downto 0);
			hex6 : out std_logic_vector(6 downto 0);
			hex7 : out std_logic_vector(6 downto 0)
		);
	end component;
end package;