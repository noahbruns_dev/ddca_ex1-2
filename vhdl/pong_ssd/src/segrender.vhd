library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity segrender is
	port(
		clk   : in std_logic;
		res_n : in std_logic;
		
		player_points : in std_logic_vector(5 downto 0);
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0)
	);
end entity;

architecture behavior of segrender is
	type seg_state is (DECREASE, WRITEOUT);
	
	signal state : seg_state := WRITEOUT;
	signal next_state : seg_state;
	
	signal tens : unsigned(5 downto 0);
	signal tens_next : unsigned(5 downto 0);
	
	signal singles: unsigned(5 downto 0);
	signal singles_next: unsigned(5 downto 0);
	
	signal local: unsigned(5 downto 0);
	signal local_next: unsigned(5 downto 0);
	
	signal local_tens: unsigned(5 downto 0);
	signal local_tens_next: unsigned(5 downto 0);
begin
	sync: process (clk, res_n, player_points, tens, singles)
	begin
		if res_n = '0' then
			state <= WRITEOUT;
			local <= (others => '0');
			local_tens <= (others => '0');
		elsif rising_edge(clk) then
			state <= next_state;
			local <= local_next;
			local_tens <= local_tens_next;
			tens <= tens_next;
			singles <= singles_next;
		end if;
	end process;
	
	nexts : process (state, local, local_tens, player_points, tens, singles)
	begin
		next_state <= state;
		local_next <= local - 10;
		local_tens_next <= local_tens + 1;
		tens_next <= tens;
		singles_next <= singles;
		
		case state is
			when WRITEOUT =>
				next_state <= DECREASE;
				local_next <= unsigned(player_points);
				local_tens_next <= (others => '0');
				tens_next <= local_tens;
				singles_next <= local;
				
			when DECREASE =>
				if local < 10 then
					next_state <= WRITEOUT;
					local_next <= local;
					local_tens_next <= local_tens;
				end if;
		end case;
	end process;
	
	with singles select hex0 <=
		"1000000" when to_unsigned(0, 6),
		"1111001" when to_unsigned(1, 6),
		"0100100" when to_unsigned(2, 6),
		"0110000" when to_unsigned(3, 6),
		"0011001" when to_unsigned(4, 6),
		"0010010" when to_unsigned(5, 6),
		"0000010" when to_unsigned(6, 6),
		"1111000" when to_unsigned(7, 6),
		"0000000" when to_unsigned(8, 6),
		"0010000" when to_unsigned(9, 6),
		"1111111" when others;
		
	with tens select hex1 <=
		"1000000" when to_unsigned(0, 6),
		"1111001" when to_unsigned(1, 6),
		"0100100" when to_unsigned(2, 6),
		"0110000" when to_unsigned(3, 6),
		"0011001" when to_unsigned(4, 6),
		"0010010" when to_unsigned(5, 6),
		"0000010" when to_unsigned(6, 6),
		"1111000" when to_unsigned(7, 6),
		"0000000" when to_unsigned(8, 6),
		"0010000" when to_unsigned(9, 6),
		"1111111" when others;
end architecture;