library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pong_pkg.all;

package segrender_pkg is
	component segrender is
		port(
			clk   : in std_logic;
			res_n : in std_logic;
			
			player_points : in std_logic_vector(5 downto 0);
			hex0 : out std_logic_vector(6 downto 0);
			hex1 : out std_logic_vector(6 downto 0)
		);
	end component;
end package;