library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pong_ssd_pkg.all;
use work.pong_pkg.all;
use work.segrender_pkg.all;

entity pong_ssd is
	generic (
		CYCLES : natural := 50_000_000
	);
	port (
		game_state : in pong_game_state_t;
		clk : in std_logic;
		res_n : in std_logic;
		
		player1_points : in std_logic_vector(5 downto 0);
		player2_points : in std_logic_vector(5 downto 0);
		
		hex0 : out std_logic_vector(6 downto 0);
		hex1 : out std_logic_vector(6 downto 0);
		hex2 : out std_logic_vector(6 downto 0);
		hex3 : out std_logic_vector(6 downto 0);
		hex4 : out std_logic_vector(6 downto 0);
		hex5 : out std_logic_vector(6 downto 0);
		hex6 : out std_logic_vector(6 downto 0);
		hex7 : out std_logic_vector(6 downto 0)
	);
end entity;

architecture behavior of pong_ssd is

	type STATE_TYPE is (SH_1, SH_2, SH_3, SH_4, SH_5, SH_6);
	signal state, state_next : STATE_TYPE;
	signal run0, run1, run2, run3 : std_logic_vector(6 downto 0);
	
begin
	--connect Seg Display
	p1_inst: segrender port map (
		clk => clk,
		res_n => res_n,
		player_points => player1_points,
		hex0 => hex6,
		hex1 => hex7
	);
	
	p2_inst: segrender port map (
		clk => clk,
		res_n => res_n,
		player_points => player2_points,
		hex0 => hex4,
		hex1 => hex5
	);
	
	
	sync: process (clk)
		variable cnt : unsigned (35 downto 0) := (others => '0');
	begin
		if rising_edge(clk) then
			if cnt = CYCLES then
				state <= state_next;
				cnt := (others => '0');
		   else
				cnt := cnt + 1;
			end if;
		end if;
	end process;
	
	output : process (state)
	begin		
		case state is
			when SH_1 =>
				run0 <= "1111001";
				run1 <= "1111111";
				run2 <= "1111111";
				run3 <= "0001100";
			when SH_2 =>
				run0 <= "1111001";
				run1 <= "1111111";
				run2 <= "0100011";
				run3 <= "1001111";
			when SH_3 =>
				run0 <= "1111001";
				run1 <= "0011100";
				run2 <= "1111111";
				run3 <= "1001111";
			when SH_4 =>
				run0 <= "0100001";
				run1 <= "1111111";
				run2 <= "1111111";
				run3 <= "1001111";
			when SH_5 =>
				run0 <= "1111001";
				run1 <= "0011100";
				run2 <= "1111111";
				run3 <= "1001111";
			when SH_6 =>
				run0 <= "1111001";
				run1 <= "1111111";
				run2 <= "0100011";
				run3 <= "1001111";
		end case;
	end process;
	
	next_state : process (state)
	begin
		state_next <= state;
		
		case state is
			when SH_1 =>
				state_next <= SH_2;
			when SH_2 =>
				state_next <= SH_3;
			when SH_3 =>
				state_next <= SH_4;
			when SH_4 =>
				state_next <= SH_5;
			when SH_5 =>
				state_next <= SH_6;
			when SH_6 =>
				state_next <= SH_1;
		end case;
	end process;

	with game_state select hex0 <=
		"0111111" when IDLE,
		run0 when RUNNING,
		"1111001" when PAUSED,
		"0001111" when PLAYER1_WON,
		"0001111" when PLAYER2_WON;
			
	with game_state select hex1 <=
		"0111111" when IDLE,
		run1 when RUNNING,
		"0111111" when PAUSED,
		"1111001" when PLAYER1_WON,
		"0100100" when PLAYER2_WON;
			
	with game_state select hex2 <=
		"0111111" when IDLE,
		run2 when RUNNING,
		"0111111" when PAUSED,
		"0001100" when PLAYER1_WON,
		"0001100" when PLAYER2_WON;
			
	with game_state select hex3 <=
		"0111111" when IDLE,
		run3 when RUNNING,
		"1001111" when PAUSED,
		"0111001" when PLAYER1_WON,
		"0111001" when PLAYER2_WON;
end architecture;