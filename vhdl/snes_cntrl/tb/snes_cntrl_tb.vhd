library ieee;
use ieee.std_logic_1164.all;
use work.snes_cntrl_pkg.all;

entity snes_tb is
end entity;


architecture beh of snes_tb is
	constant CLK_PERIOD : time := 20 ns;
	signal stop_clock : boolean := false;
	signal clk      : std_logic;

	signal snes_latch : std_logic;
	signal snes_clk : std_logic;
	signal snes_data : std_logic;
begin

	top_inst : snes_cntrl port map(
		clk => clk,
		res_n => '1',
		snes_data => snes_data,
		snes_latch => snes_latch,
		snes_clk => snes_clk
	);
	-----------------------------------------------
	--   Instantiate your top level entity here  --
	-----------------------------------------------
	-- The testbench generates the above singals --
	-- use them as inputs to your system         --
	-----------------------------------------------

	stimulus : process
	begin
		wait until rising_edge(snes_latch);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);


		snes_data <= '0';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '0';
		wait until rising_edge(snes_clk);

		snes_data <= '0';
		wait until rising_edge(snes_clk);


		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);

		snes_data <= '0';
		wait until rising_edge(snes_clk);

		snes_data <= '1';
		wait until rising_edge(snes_clk);
	end process;

	generate_clk : process
	begin
		while not stop_clock loop
			clk <= '0', '1' after CLK_PERIOD / 2;
			wait for CLK_PERIOD;
		end loop;
		wait;
	end process;

end architecture;
