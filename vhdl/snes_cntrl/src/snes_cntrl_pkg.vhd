library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package snes_cntrl_pkg is

	type snes_buttons_t is record
		btn_up     : std_logic;
		btn_down   : std_logic;
		btn_left   : std_logic;
		btn_right  : std_logic;
		btn_a      : std_logic;
		btn_b      : std_logic;
		btn_x      : std_logic;
		btn_y      : std_logic;
		btn_l      : std_logic;
		btn_r      : std_logic;
		btn_start  : std_logic;
		btn_select : std_logic;
	end record;

	constant SNES_BUTTONS_RESET_VALUE : snes_buttons_t := (others=>'0');
	
	
	component snes_cntrl is
		port (
			clk   : in std_logic;
			res_n : in std_logic;
			
			snes_latch 		: out std_logic;
			snes_clk			: out std_logic;
			snes_data		: in std_logic;
			button_state	: out snes_buttons_t
		);
	end component;

--	component fake_snes_cntrl is
--		generic (
--			SYNC_STAGES : integer := 2
--		);
--		port (
--			clk : in std_logic;
--			res_n : in std_logic;
--			switches : in std_logic_vector(11 downto 0);
--			button_state : out snes_buttons_t
--		);
--	end component;
end package;

