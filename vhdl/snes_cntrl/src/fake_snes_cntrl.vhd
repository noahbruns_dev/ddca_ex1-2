library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.snes_cntrl_pkg.all;
use work.sync_pkg.all;

entity fake_snes_cntrl is
	generic (
		SYNC_STAGES : integer := 2
	);
	port (
		clk : in std_logic;
		res_n : in std_logic; 
		
		-- use the board switches to emulate the snes buttons
		switches : std_logic_vector(11 downto 0);
		
		-- button outputs
		button_state : out snes_buttons_t
	);
end entity;

architecture rtl of fake_snes_cntrl is
	signal switches_sync : std_logic_vector(11 downto 0);
begin
	GENERATE_SYNCHRONIZERS : for i in 0 to 11 generate
		sync_inst : sync
		generic map (
			SYNC_STAGES => SYNC_STAGES,
			RESET_VALUE => '0'
		)
		port map (
			clk      => clk,
			res_n    => res_n,
			data_in  => switches(i),
			data_out => switches_sync(i)
		);
	end generate;
	
	button_state.btn_up     <= switches_sync(0);
	button_state.btn_down   <= switches_sync(1);
	button_state.btn_left   <= switches_sync(2);
	button_state.btn_right  <= switches_sync(3);
	button_state.btn_a      <= switches_sync(4);
	button_state.btn_b      <= switches_sync(5);
	button_state.btn_x      <= switches_sync(6);
	button_state.btn_y      <= switches_sync(7);
	button_state.btn_start  <= switches_sync(8);
	button_state.btn_select <= switches_sync(9);
	button_state.btn_l      <= switches_sync(10);
	button_state.btn_r      <= switches_sync(11);
end architecture;

