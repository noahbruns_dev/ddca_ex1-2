library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.snes_cntrl_pkg.all;
use work.sync_pkg.all;

entity snes_cntrl is
	generic(
		CLK_FREQ 			: integer := 50_000_000;
		CLK_OUT_FREQ    	: integer := 1_000_000;
		REFRESH_TIMEOUT   : integer := 400000
	);
	port (
		clk   : in std_logic;
		res_n : in std_logic;
		
		snes_latch 		: out std_logic;
		snes_clk			: out std_logic;
		snes_data		: in std_logic;
		button_state	: out snes_buttons_t
	);
end entity;

architecture arch of snes_cntrl is 
	type snes_state is (WAIT_TIMEOUT, LATCH, LATCH_WAIT, CLK_LOW, SAMPLE, CLK_HIGH);
	
	constant BIT_TIME			: integer := CLK_FREQ / CLK_OUT_FREQ;
	constant BIT_TIME_HALF 	: integer := BIT_TIME / 2;

	signal state : snes_state := WAIT_TIMEOUT;
	signal next_state : snes_state := WAIT_TIMEOUT;
	
	signal clk_cnt : unsigned (19 downto 0) := (others => '0');
	signal next_clk_cnt : unsigned (19 downto 0);
	
	signal bit_cnt : unsigned (3 downto 0) := (others => '0');
	signal next_bit_cnt : unsigned (3 downto 0);
	
	signal buf : std_logic_vector(15 downto 0);
	signal next_buf : std_logic_vector(15 downto 0);
	
	signal next_button_state : snes_buttons_t;
begin

	sync: process (clk, res_n)
	begin
		if res_n = '0' then
			state <= WAIT_TIMEOUT;
			clk_cnt <= (others => '0');
			bit_cnt <= (others => '0');
			buf <= (others => '0');
			button_state <= SNES_BUTTONS_RESET_VALUE;
		elsif rising_edge(clk) then
			state <= next_state;
			clk_cnt <= next_clk_cnt;
			bit_cnt <= next_bit_cnt;
			buf <= next_buf;
			button_state <= next_button_state;
		end if;
	end process;
	
	output : process (state, bit_cnt)
	begin		
		case state is
			when WAIT_TIMEOUT =>
				snes_latch <= '0';
				snes_clk <= '1';				
			when LATCH =>
				snes_latch <= '1';
				snes_clk <= '1';
			when LATCH_WAIT =>
				snes_latch <= '0';
				snes_clk <= '1';
			when CLK_LOW =>
				snes_latch <= '0';
				snes_clk <= '0';
			when SAMPLE =>
				snes_latch <= '0';
				snes_clk <= '0';
			when CLK_HIGH =>
				snes_latch <= '0';
				snes_clk <= '1';
		end case;
	end process;
	
	nexts : process (state, clk_cnt, buf, snes_data, bit_cnt)
	begin
		next_state <= state;
		next_clk_cnt <= clk_cnt + 1;
		next_bit_cnt <= bit_cnt;
		next_buf <= buf;
		next_button_state <= button_state;
		
		case state is
			when WAIT_TIMEOUT =>
				if (clk_cnt = REFRESH_TIMEOUT) then
					next_clk_cnt <= (others => '0');
					next_state <= LATCH;
				end if;
				
			when LATCH =>
				if (clk_cnt = BIT_TIME_HALF) then
					next_clk_cnt <= (others => '0');
					next_state <= LATCH_WAIT;
				end if;
				
			when LATCH_WAIT =>
				if (clk_cnt = BIT_TIME_HALF) then
					next_clk_cnt <= (others => '0');
					next_state <= CLK_LOW;
				end if;
				
			when CLK_LOW =>
				if (clk_cnt = BIT_TIME_HALF - 1) then
					next_clk_cnt <= (others => '0');
					next_state <= SAMPLE;
				end if;
				
			when SAMPLE =>
				next_state <= CLK_HIGH;
				next_buf(14 downto 0) <= buf(15 downto 1);
				next_buf(15) <= not snes_data;
				
			when CLK_HIGH =>
				if (clk_cnt = BIT_TIME_HALF) then
					if (bit_cnt = 15) then
						next_clk_cnt <= (others => '0');
						next_bit_cnt <= (others => '0');
						next_state <= WAIT_TIMEOUT;
						
						next_button_state.btn_up 		<= buf(4);
						next_button_state.btn_down 	<= buf(5);
						next_button_state.btn_left 	<= buf(6);
						next_button_state.btn_right 	<= buf(7);
						next_button_state.btn_a 		<= buf(8);
						next_button_state.btn_b 		<= buf(0);
						next_button_state.btn_x 		<= buf(9);
						next_button_state.btn_y 		<= buf(1);
						next_button_state.btn_l 		<= buf(10);
						next_button_state.btn_r 		<= buf(11);
						next_button_state.btn_start 	<= buf(3);
						next_button_state.btn_select  <= buf(2);
						
					else
						next_clk_cnt <= (others => '0');
						next_bit_cnt <= bit_cnt + 1;
						next_state <= CLK_LOW;
					end if;
				end if;	
		end case;
	end process;
end architecture;
