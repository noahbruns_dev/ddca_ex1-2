#!/bin/bash

dirs_with_no_vhdl_files="
vhdl
vhdl/top
vhdl/top/quartus
vhdl/pong
vhdl/pong/scripts
vhdl/pong_ssd
vhdl/pong_ssd/scripts
vhdl/audio_cntrl
vhdl/audio_cntrl/scripts
vhdl/snes_cntrl
vhdl/snes_cntrl/scripts"

ipcores="
graphics_controller
math
pong
pong_ssd
ram
serial_port
snes_cntrl
synchronizer
top
uart_snes_bridge
"


testbenches="
top
pong_ssd
snes_cntrl"


error_counter=0
warning_counter=0


if [ ! -d vhdl ]; then 
	echo "Error: vhdl directory missing"; 
	let "error_counter++"
fi;
	
for core in $ipcores; do 
	if [ ! -d vhdl/$core ]; then 
		echo "Error: module [$core] missing from your source tree"; 
		let "error_counter++"
	fi;
done;

for i in $dirs_with_no_vhdl_files; do 
	for f in $i/*.vhd; do 
		if [ -e "$f" ]; then
			echo "Error: [$i] -- This folder MUST NOT contain VHDL files. VHDL files may only be put in the src folder of each submodule"; 
			let "error_counter++"
		fi;
		break;
	done;
done;

for i in $testbenches; do 
	#echo "vhdl/$i/tb/*.vhd";
	for f in vhdl/$i/tb/*.vhd; do 
		if [ ! -e "$f" ]; then
			echo "Warning: No testbech found for module [$i]";
			let "warning_counter++"
		fi;
		break;
	done;
done;

#check for top makefile
if [ ! -e vhdl/top/Makefile ]; then
	echo "Warning: Makefile for top module is missing!";
	let "warning_counter++"
fi;

#check for quartus project
for f in vhdl/top/quartus/*.qpf; do 
	if [ ! -e "$f" ]; then
		echo "Error: Quartus project missing!";
		let "error_counter++"
	fi;
	break;
done;

#check for SDC file in quartus project directory
for f in vhdl/top/quartus/*.sdc; do 
	if [ ! -e "$f" ]; then
		echo "Warning: SDC file missing in top-level quartus project directory vhdl/top/quartus!";
		let "warning_counter++"
	fi;
	break;
done;

for f in vhdl/pong_ssd/src/*.vhd; do 
	if [ -e "$f" ]; then
		if [ $(cat vhdl/pong_ssd/src/*.vhd | grep process | grep all | wc -l) -ne 0 ]; then 
			echo "Warning: Your pong_ssd seems to use the all keyword for sensitivity lists";
		fi;
	fi;
	break;
done;

exit $error_counter

